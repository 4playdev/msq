<?php
    
$_['heading_title']  = 'MSQ Карточки';
    
// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_title']      = 'Заголовок';
$_['entry_banner']     = 'Баннер';
$_['entry_link']       = 'Ссылка на Instagram';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';
$_['error_name']       = 'Название должно содержать от 3 до 64 символов!';
$_['error_title']      = 'Введите заголовок';
$_['error_banner']     = 'Выберите баннер';
$_['error_link']       = 'Введите ссылку';
