<?php
// Heading
$_['heading_title'] = 'Жеке кабинетіне';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_my_account'] = 'Пайдаланушының есебі';
$_['text_my_orders'] = 'Менің тапсырыстарым';
$_['text_my_newsletter'] = 'Жазылу';
$_['text_edit'] = 'Негізгі деректер';
$_['text_password'] = 'Өзгерте аласыз құпия сөз';
$_['text_address'] = 'Өзгерте менің мекен-жайы';
$_['text_credit_card'] = 'Менеджер төлем карталарын';
$_['text_wishlist'] = 'Өзгертуді қалау';
$_['text_order'] = 'Тарих тапсырыстарды';
$_['text_download'] = 'Файлдарды жүктеу';
$_['text_reward'] = 'Бонустық баллдар';
$_['text_return'] = 'Өтінімді қайтару';
$_['text_transaction'] = 'Тарих төлемдер';
$_['text_newsletter'] = 'Өңдеу жазылуды';
$_['text_recurring'] = 'Тұрақты төлемдер';
$_['text_transactions'] = 'Аудару';