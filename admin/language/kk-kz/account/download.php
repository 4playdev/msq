<?php
// Heading
$_['heading_title'] = 'Файлдарды жүктеу';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_downloads'] = 'Файлдарды жүктеу';
$_['text_empty'] = 'Сізде ешқандай тапсырыстарды файлдарды жүктеу үшін!';

// Column
$_['column_order_id'] = '№ тапсырыс:';
$_['column_name'] = 'Аты:';
$_['column_size'] = 'Мөлшері:';
$_['column_date_added'] = 'Қосу күні:';
