<?php
// Heading
$_['heading_title'] = 'Тіркелгі';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_edit'] = 'Өңдеу ақпарат';
$_['text_your_details'] = 'Тіркелгіңіз';
$_['text_success'] = 'Тіркелгіңіз сәтті жаңартылды!';

// Entry
$_['entry_firstname'] = 'Аты';
$_['entry_lastname'] = 'Аты-жөні';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Телефоны';
$_['entry_fax'] = 'Факс';

// Error
$_['error_exists'] = 'Осындай E-Mail тіркелген!';
$_['error_firstname'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_lastname'] = 'Тегі болуы тиіс 1-ден 32 символдан!';
$_['error_email'] = 'E-Mail мекен-жайы енгізілді дұрыс емес!';
$_['error_telephone'] = 'Телефон нөмірі болуы тиіс 3-тен 32 символдан!';
$_['error_custom_field'] = '%s Кажет!';
