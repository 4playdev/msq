<?php
// Heading
$_['heading_title'] = 'Авторизациялау';

// Text
$_['text_account'] = 'Жеке габинет';
$_['text_login'] = 'Авторизациялау';
$_['text_new_customer'] = 'Жаңа қолданушы';
$_['text_register'] = 'Тіркеу';
$_['text_register_account'] = 'Тіркелгіні Жасау көмектеседі істеу келесі сатып алу жылдам (қажеті жоқ қайтадан енгізуге мекен-жайы және байланыс ақпаратты), көру тапсырыс күйі, сондай-ақ көруге тапсырыстар, жасалған бұрын. Сіз сондай-ақ сіз жинақтау кезінде тексеріңіз жүлделі ұпайлар (оларға да болады бір нәрсе сатып алу), тұрақты сатып алушыларға, біз, жеңілдіктер жүйесін.';
$_['text_returning_customer'] = 'Мен қазірдің өзінде тіркелген жатырмын';
$_['text_i_am_returning_customer'] = 'Авторизациялау';
$_['text_forgotten'] = 'Парольді Ұмыттыңыз ба?';

// Entry
$_['entry_email'] = 'E-Mail:';
$_['entry_password'] = 'Құпия сөз:';

// Error
$_['error_login'] = 'Дұрыс емес толтырылған өріс E-Mail және/немесе пароль!';
$_['error_attempts'] = 'Назар салыңыз: Сіз асып рұқсат етілген саны әрекеттерінің жүйесіне кіру. Көріңіз бір сағат.';
$_['error_approved'] = 'Қажет тіркелгіні алдында авторизацией.';