<?php
// Heading
$_['heading_title'] = 'Жаңалықтарға Жазылу';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_newsletter'] = 'Жіберу';
$_['text_success'] = 'Сіздің жазылу сәтті жаңартылды!';

// Entry
$_['entry_newsletter'] = 'Жазылу:';