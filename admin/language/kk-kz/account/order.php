<?php
// Heading
$_['heading_title'] = 'Тарих тапсырыстарды';

// Button
$_['button_ocstore_payeer_onpay'] = 'Онлайн';
$_['button_ocstore_yk_onpay'] = 'Онлайн';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_order'] = 'Тапсырысы';
$_['text_order_detail'] = 'Бөлшектер тапсырысты';
$_['text_invoice_no'] = ' Шот№:';
$_['text_order_id'] = '№ Тапсырыс:';
$_['text_date_added'] = 'Filed:';
$_['text_shipping_address'] = 'жеткізу Мекен-жайы';
$_['text_shipping_method'] = 'жеткізу Тәсілі:';
$_['text_payment_address'] = 'Төлем мекен-жайы';
$_['text_payment_method'] = 'Төлеу тәсілі:';
$_['text_comment'] = 'Тапсырысқа түсініктеме';
$_['text_history'] = 'Тарих тапсырыстарды';
$_['text_success'] = ') Тауарды тапсырыс <a href="%s">%s</a> қосылды <a href="%s">Сіздің себетке</a>!';
$_['text_empty'] = 'Сіз әлі жасамаған сауда!';
$_['text_error'] = 'Сұраған тапсырысты табылмады!';

// Column
$_['column_order_id'] = '№ Тапсырыс';
$_['column_customer'] = 'Алушы';
$_['column_product'] = 'Кол-во тауардың';
$_['column_name'] = 'Атауы';
$_['column_model'] = 'Үлгі';
$_['column_quantity'] = 'Саны';
$_['column_price'] = 'Баға';
$_['column_total'] = 'Барлығы';
$_['column_action'] = 'Әрекет';
$_['column_date_added'] = 'Filed';
$_['column_status'] = 'Мәртебе тапсырысты';
$_['column_comment'] = 'Пікір';

// Error
$_['error_reorder'] = '%s Казіргі уақытта қол жетімді үшін тапсырыс.';