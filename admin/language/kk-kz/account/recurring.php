<?php
// Heading
$_['heading_title'] = 'Ағымдық төлемдер';

// Text
$_['text_account'] = 'Account';
$_['text_recurring'] = 'Тұрақты төлемдер';
$_['text_recurring_detail'] = 'Бөлшектер тұрақты төлем';
$_['text_order_recurring_id'] = 'Recurring ID:';
$_['text_date_added'] = 'Құрылды:';
$_['text_status'] = 'Мәртебе:';
$_['text_payment_method'] = 'Төлем әдісі:';
$_['text_order_id'] = 'Тапсырыс ID:';
$_['text_product'] = 'Тауар:';
$_['text_quantity'] = 'Саны:';
$_['text_description'] = 'Сипаттамасы';
$_['text_reference'] = 'Ескерту';
$_['text_transaction'] = 'Аудару';


$_['text_status_1'] = 'Белсенділігі';
$_['text_status_2'] = 'Белсенді емес';
$_['text_status_3'] = 'Күші';
$_['text_status_4'] = 'Ерітілген';
$_['text_status_5'] = 'Аяқталса';
$_['text_status_6'] = 'Күтеді';

$_['text_transaction_date_added'] = 'Жасалды';
$_['text_transaction_payment'] = 'Төлем';
$_['text_transaction_outstanding_payment'] = 'Төлем түспеген';
$_['text_transaction_skipped'] = 'Төлем өтіп кетсе';
$_['text_transaction_failed'] = 'Проблемасы төлеумен';
$_['text_transaction_cancelled'] = 'Күші';
$_['text_transaction_suspended'] = 'Ерітілген';
$_['text_transaction_suspended_failed'] = 'Ерітілген үшін сәтсіз төлемнің';
$_['text_transaction_outstanding_failed'] = 'Төлем күні';
$_['text_transaction_expired'] = 'Аяқталса';




$_['text_empty'] = 'сізде бірде-бір саладағы мерзімдік төлеммен';
$_['text_error'] = 'Төлем сіз сұраған жоқ!';








$_['text_cancelled'] = 'Тұрақты төлем жабылды';

// Column
$_['column_date_added'] = 'Жасалды';
$_['column_type'] = 'Түрі';
$_['column_amount'] = 'Сомасы';
$_['column_status'] = 'Мәртебе';
$_['column_product'] = 'Тауарға';
$_['column_order_recurring_id'] = 'Бейіні n';

// Error
$_['error_not_cancelled'] = 'Қате: ' %s'';
$_['error_not_found'] = 'Болмайды өшіру профиль';

// Button
$_['button_return'] = 'Қайтару';
