<?php
// Heading
$_['heading_title'] = 'Тіркеу';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_register'] = 'Тіркеу';
$_['text_account_already'] = 'Егер сіз тіркелген, бетіне өтіңіз <a href="%s">авторизация</a>.';
$_['text_your_details'] = 'Негізгі деректер';
$_['text_your_address'] = 'Сіздің мекен-жайы';
$_['text_newsletter'] = 'Жаңалықтарға жазылу';
$_['text_your_password'] = 'Сіздің пароль';
$_['text_agree'] = 'Менімен оқылды, мен келісімімді беремін құжатпен <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Тобы сатып алушылардың';
$_['entry_firstname'] = 'Аты';
$_['entry_lastname'] = 'Аты-жөні';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Телефондары';
$_['entry_fax'] = 'Факс';
$_['entry_company'] = 'Компания';
$_['entry_address_1'] = 'Мекен жайы';
$_['entry_address_2'] = 'Мекен-жайы (қосымша)';
$_['entry_postcode'] = 'Индексі';
$_['entry_city'] = 'Қала';
$_['entry_country'] = 'Ел';
$_['entry_zone'] = 'Аймақ / облыс';
$_['entry_newsletter'] = 'Жазылу';
$_['entry_password'] = 'Пароль';
$_['entry_confirm'] = 'Растау';

// Error
$_['error_exists'] = 'Бұл E-Mail тіркелген!';
$_['error_firstname'] = 'Аты болуы тиіс 1-ден 32 символдан!';
$_['error_lastname'] = 'Тегі болуы тиіс 1-ден 32 символдан!';
$_['error_email'] = 'E-Mail енгізілді дұрыс емес!';
$_['error_telephone'] = 'Телефонда болуы тиіс 3-тен 32 цифрлардан!';
$_['error_address_1'] = 'Мекен-жайы болуы тиіс 3-тен 128 таңбадан!';
$_['error_city'] = 'Кала Атауы болуы тиіс 2-ден 128 таңбадан!';
$_['error_postcode'] = 'Индексінде болуы тиіс 2-ден 10-рәміздер!';
$_['error_country'] = 'Елді Таңдаңыз!';
$_['error_zone'] = 'Таңдаңыз аймақ / область!';
$_['error_custom_field'] = '%s қажет!';
$_['error_password'] = 'Бұл пароль болуы тиіс 4-ден 20 мәнге дейін!';
$_['error_confirm'] = 'Парольдер сәйкес келмейді!';
$_['error_agree'] = 'Үшін тіркеу керек келіседі құжаты %s!';
