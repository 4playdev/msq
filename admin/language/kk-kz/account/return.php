<?php
// Heading
$_['heading_title'] = 'Тауарды Қайтару';

// Text
$_['text_account'] = 'Жеке Кабинетіне';
$_['text_return'] = 'Кайтаруы туралы Ақпарат,';
$_['text_return_detail'] = 'Негізгі деректер';
$_['text_description'] = 'форманы толтырыңыз тауарды қайтаруға.';
$_['text_order'] = 'Ақпарат тапсырыс туралы';
$_['text_product'] = 'Тауар туралы Ақпарат және қайтару себебі';
$_['text_reason'] = 'Кайтару Себебі';
$_['text_message'] = '<p>Сіз жіберді өтінім тауардың қайтарылуы</p><p> мәртебесі туралы Хабарламаны өтінім келетін болады сіздің e-mail. Рахмет!</p>';
$_['text_return_id'] = '№ Қайтару';
$_['text_order_id'] = '№ Тапсырыс';
$_['text_date_ordered'] = 'Дата заказа';
$_['text_status'] = 'Мәртебе';
$_['text_date_added'] = 'Өтініш берілген Күні';
$_['text_comment'] = 'Пікір';
$_['text_history'] = 'Тарих қайтару';
$_['text_empty'] = 'Сізде ешқандай бұрын қайтаруды тауарлар!';
$_['text_agree'] = 'Менімен оқылды, мен келісімімді беремін құжатпен <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id'] = '№ Қайтару:';
$_['column_order_id'] = '№ Тапсырыс:';
$_['column_status'] = 'Мәртебе';
$_['column_date_added'] = 'Өтініш берілген Күні:';
$_['column_customer'] = 'Сатып Алушы:';
$_['column_product'] = 'Атауы:';
$_['column_model'] = 'Моделі:';
$_['column_quantity'] = 'Саны:';
$_['column_price'] = 'Баға';
$_['column_opened'] = 'Өтініш ашылды:';
$_['column_comment'] = 'Пікір';
$_['column_reason'] = 'қайтару Себебі';
$_['column_action'] = 'Әрекет';

// Entry
$_['entry_order_id'] = '№ тапсырыс:';
$_['entry_date_ordered'] = 'Тапсырыс күні:';
$_['entry_firstname'] = 'Аты:';
$_['entry_lastname'] = 'Аты:';
$_['entry_email'] = 'E-Mail:';
$_['entry_telephone'] = 'Телефон:';
$_['entry_product'] = 'Атауы:';
$_['entry_model'] = 'Моделі:';
$_['entry_quantity'] = 'Саны:';
$_['entry_reason'] = 'Себебі:';
$_['entry_opened'] = 'Сығымдан:';
$_['entry_fault_detail'] = 'Сипаттамасы:';

// Error
$_['text_error'] = 'Сіздің сұрауыңыз Бойынша ештеңе табылған!';
$_['error_order_id'] = 'Көрсетілмесе, № тапсырыс!';
$_['error_firstname'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_lastname'] = 'Тегі болуы тиіс 1-ден 32 символдан!';
$_['error_email'] = 'E-Mail мекен-жайы енгізілді дұрыс емес!';
$_['error_telephone'] = 'Телефон Нөмірі болуы тиіс 3-тен 32 символдан!';
$_['error_product'] = 'Тауардың Атауы болуы тиіс 3-тен 255 символдан!';
$_['error_model'] = 'Модель Атауы болуы тиіс 3-тен 64 символдан!';
$_['error_reason'] = 'Қажет (себебін көрсету тауарды қайтару!';
$_['error_agree'] = 'Ресімдеу Үшін кері қайтару керек келіседі құжатқа %s!';
