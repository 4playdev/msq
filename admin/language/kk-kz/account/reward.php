<?php
// Heading
$_['heading_title'] = 'Бонустық баллдар';

// Column
$_['column_date_added'] = 'Есептеу Күні';
$_['column_description'] = 'Есептелуі үшін:';
$_['column_points'] = 'Жинаған Ұпай';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_reward'] = 'Бонустық баллдар';
$_['text_total'] = 'Жинақталған бонустық ұпай:';
$_['text_empty'] = 'Сізде бонустық ұпай!';