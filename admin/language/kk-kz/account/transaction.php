<?php
// Heading
$_['heading_title'] = 'Тарих төлемдер';

// Column
$_['column_date_added'] = 'Filed';
$_['column_description'] = 'Сипаттамасы';
$_['column_amount'] = 'Сомасы (%s)';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_transaction'] = 'Сіздің төлемдер';
$_['text_total'] = 'Сіздің ағымдағы балансы:';
$_['text_empty'] = 'Сізде ешқандай төлемдер!';