<?php
// Heading
$_['heading_title'] = 'Менің отырғызу';

// Text
$_['text_account'] = 'Жеке кабинетіне';
$_['text_instock'] = 'Бар';
$_['text_wishlist'] = 'Отырғызу (%s)';
$_['text_login'] = 'Кіру кажет <a href="%s">Жеке кабинетіне</a> немесе <a href="%s">тіркелгі</a> қосу үшін тауар <a href="%s">%s</a> өз <a href="%s">бетбелгілер</a>!';
$_['text_success'] = 'Тауарға <a href="%s">%s</a> сәтті қосылды <a href="%s">бетбелгілер</a>!';
$_['text_remove'] = 'Тізім бетбелгі сәтті жаңартылды!';
$_['text_empty'] = 'Сіздің бетбелгілер пусты';

// Column
$_['column_image'] = 'Image';
$_['column_name'] = 'Атауы';
$_['column_model'] = 'Үлгі';
$_['column_stock'] = 'Коймада';
$_['column_price'] = 'Баға';
$_['column_action'] = 'Әрекет';