<?php
// Heading
$_['heading_title'] = 'Мәліметтер есептік жазбасын';

// Text
$_['text_account'] = 'Кабинеті серіктес';
$_['text_edit'] = 'Өңдеу ақпарат';
$_['text_your_details'] = 'Сіздің жеке деректер';
$_['text_your_address'] = 'Сіздің мекен-жайы';
$_['text_success'] = 'тіркелгіңіз сәтті жаңартылды.';

// Entry
$_['entry_firstname'] = 'Аты';
$_['entry_lastname'] = 'Аты-жөні';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Телефоны';
$_['entry_fax'] = 'Факс';
$_['entry_company'] = 'Компания';
$_['entry_website'] = 'Веб-сайт';
$_['entry_address_1'] = 'Мекен жайы';
$_['entry_address_2'] = 'Мекен-жайы (жалғасы)';
$_['entry_postcode'] = 'Индексі';
$_['entry_city'] = 'Қала';
$_['entry_country'] = 'Ел';
$_['entry_zone'] = 'Аймақ / облыс';

// Error
$_['error_exists'] = 'Осындай E-Mail тіркелген!';
$_['error_firstname'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_lastname'] = 'Тегі болуы тиіс 1-ден 32 символдан!';
$_['error_email'] = 'E-Mail мекен-жайы енгізілді дұрыс емес!';
$_['error_telephone'] = 'телефон Нөмірі болуы тиіс 3-тен 32 символдан!';
$_['error_address_1'] = 'Мекен-жайы болуы тиіс 3-тен 128 таңбадан!';
$_['error_city'] = ', қала Атауы болуы тиіс 2-ден 128 таңбадан!';
$_['error_country'] = 'таңдаңыз елге!';
$_['error_zone'] = 'таңдаңыз аймақ / облыс';
$_['error_postcode'] = 'Индексі тиіс 2-ден 10 символ!';