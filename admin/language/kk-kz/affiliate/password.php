<?php
// Heading
$_['heading_title'] = 'парольді Ауыстыру';

// Text
$_['text_account'] = 'Кабинеті серіктес';
$_['text_password'] = 'Сіздің пароль';
$_['text_success'] = 'Сіздің құпия сөзіңіз сәтті өзгертілді!';

// Entry
$_['entry_password'] = 'Пароль';
$_['entry_confirm'] = 'Растау';

// Error
$_['error_password'] = 'Кілтсөз болуы мүмкін 4-тен 20 рәміздер!';
$_['error_confirm'] = 'Парольдер сәйкес келмейді!';