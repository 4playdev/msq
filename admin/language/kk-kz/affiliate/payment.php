<?php
// Heading
$_['heading_title'] = 'төлеу Тәсілі';

// Text
$_['text_account'] = 'Кабинеті серіктес';
$_['text_payment'] = 'Тоқсан';
$_['text_your_payment'] = 'Сіздің аудару';
$_['text_your_password'] = 'Пароль';
$_['text_cheque'] = 'Чек';
$_['text_paypal'] = 'PayPal';
$_['text_bank'] = 'Банктік аударым';
$_['text_success'] = 'Тіркелгі деректері серіктес сәтті жаңартылды.';

// Entry
$_['entry_tax'] = ', Салық коды:';
$_['entry_payment'] = 'төлеу Тәсілі:';
$_['entry_cheque'] = 'Чекті алушының Атына төлем:';
$_['entry_paypal'] = 'PayPal Email шот:';
$_['entry_bank_name'] = 'банктің Аты:';
$_['entry_bank_branch_number'] = 'ABA/BSB нөмірі (бөлімшенің нөмірі):';
$_['entry_bank_swift_code'] = 'SWIFT код:';
$_['entry_bank_account_name'] = 'шоттың Атауы:';
$_['entry_bank_account_number'] = 'есеп шот Нөмірі:';