<?php
// Heading
$_['heading_title'] = 'Серіктестік бағдарлама';

// Text
$_['text_account'] = 'Кабинеті серіктес';
$_['text_register'] = 'Тіркеу серіктес';
$_['text_account_already'] = 'Егер серіктестік логин пожалуйста <a href="%s">авторизациялаудан өту</a>.';
$_['text_signup'] = 'жаңа тіркелгі жасау Үшін, серіктес нысанын толтыру қажет тіркеліңіз:';
$_['text_your_details'] = 'Сіздің жеке деректер';
$_['text_your_address'] = 'Сіздің мекен-жайы';
$_['text_payment'] = 'Төлем деректемелері';
$_['text_your_password'] = 'Пароль';
$_['text_cheque'] = 'Чек';
$_['text_paypal'] = 'PayPal';
$_['text_bank'] = 'Банктік аударым';
$_['text_agree'] = 'Менімен оқылды, мен келісімімді беремін құжатпен <a class="agree" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_firstname'] = 'Аты';
$_['entry_lastname'] = 'Аты-жөні';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Телефоны';
$_['entry_fax'] = 'Факс';
$_['entry_company'] = 'Компания';
$_['entry_website'] = 'Веб-сайт';
$_['entry_address_1'] = 'Мекен жайы';
$_['entry_address_2'] = 'Мекен-жайы (жалғасы)';
$_['entry_postcode'] = 'Индексі';
$_['entry_city'] = 'Қала';
$_['entry_country'] = 'Ел';
$_['entry_zone'] = 'Аймақ / облыс';
$_['entry_tax'] = 'КЕЛЕСІ';
$_['entry_payment'] = 'Әдісі төлемдерді';
$_['entry_cheque'] = 'алушының Аты';
$_['entry_paypal'] = 'PayPal аккаунт Email';
$_['entry_bank_name'] = ', банктің Атауы';
$_['entry_bank_branch_number'] = 'ABA/BSB нөмірі (бөлімшенің нөмірі)';
$_['entry_bank_swift_code'] = 'SWIFT коды';
$_['entry_bank_account_name'] = 'шоттың Аты';
$_['entry_bank_account_number'] = 'есеп-шот Нөмірі';
$_['entry_password'] = 'Пароль';
$_['entry_confirm'] = 'Растау';

// Error
$_['error_exists'] = 'Осындай E-Mail тіркелген!';
$_['error_firstname'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_lastname'] = 'Тегі болуы тиіс 1-ден 32 символдан!';
$_['error_email'] = 'E-Mail мекен-жайы енгізілді дұрыс емес!';
$_['error_telephone'] = 'телефон Нөмірі болуы тиіс 3-тен 32 символдан!';
$_['error_password'] = 'Кілтсөз болуы мүмкін 4-тен 20 рәміздер!';
$_['error_confirm'] = 'Парольдер сәйкес келмейді!';
$_['error_address_1'] = 'Мекен-жайы болуы тиіс 3-тен 128 таңбадан!';
$_['error_city'] = ', қала Атауы болуы тиіс 2-ден 128 таңбадан!';
$_['error_country'] = 'таңдаңыз елге!';
$_['error_zone'] = 'таңдаңыз аймақ / область!';
$_['error_postcode'] = 'Индексі тиіс 2-ден 10 символ!';
$_['error_agree'] = 'Сіз тиіс шарттармен келіссеңіз %s!';
