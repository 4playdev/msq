<?php
// Heading
$_['heading_title'] = 'Тарих төлемдерді';

// Column
$_['column_date_added'] = 'Filed';
$_['column_description'] = 'Сипаттамасы';
$_['column_amount'] = 'Сомасы (%s)';

// Text
$_['text_account'] = 'Кабинеті серіктес';
$_['text_transaction'] = 'Жәрдемақы';
$_['text_balance'] = 'Сіздің ағымдағы балансы:';
$_['text_empty'] = 'сізде ешқандай төлем!';
