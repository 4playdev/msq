<?php
// Heading
$_['heading_title'] = 'Себет сатып алу';

// Text
$_['text_success'] = 'Тауарға <a href="%s">%s</a> қосылды сіздің <a href="%s">себетке</a>!';
$_['text_remove'] = 'Себет сәтті жаңартылды!';
$_['text_login'] = 'қажет <a href="%s">тіркелу</a> немесе <a href="%s">создать аккаунт</a> бағаларды қарау үшін!';
$_['text_items'] = '%s тауарды(ларды) - %s';
$_['text_points'] = 'Жүлделі ұпайлар: %s';
$_['text_next'] = 'Пайдаланыңыз қосымша мүмкіндіктері бар';
$_['text_next_choice'] = 'сіз купондық коды жеңілдік немесе бонус ұпайлары сіз пайдаланғыңыз келсе, таңдаңыз тиісті тармағы. <br>Сондай-ақ, болады (шамамен) білуге жеткізу құны сіздің өңірге.';
$_['text_empty'] = 'себетте бос';
$_['text_day'] = '';
$_['text_week'] = 'апта';
$_['text_semi_month'] = 'мұсылмандарға';
$_['text_month'] = 'ай';
$_['text_year'] = 'жыл';
$_['text_trial'] = '%s әрбір %s %s %s төлеу ';
$_['text_recurring'] = '%s әрбір %s %s';
$_['text_length'] = 'үшін %s төл';
$_['text_until_cancelled'] = 'күшін жою';
$_['text_recurring_item'] = 'Қайталанатын';
$_['text_payment_recurring'] = 'Профиль төлемнің';
$_['text_trial_description'] = '%s әрбір %d %s(s) %d төлемді(оған) сол кезде';
$_['text_payment_description'] = '%s әрбір %d %s(s) %d төлемді(оған)';
$_['text_payment_cancel'] = '%s әрбір %d %s(s), ол жойылды';

// Column
$_['column_image'] = 'Image';
$_['column_name'] = 'Атауы';
$_['column_model'] = 'Үлгі';
$_['column_quantity'] = 'Саны';
$_['column_price'] = 'Баға шт.';
$_['column_total'] = 'Барлығы';

// Error
$_['error_stock'] = 'Өнімдері белгіленген *** жоқ қажетті саны немесе олардың жоқ!';
$_['error_minimum'] = 'ең Аз саны үшін тауарды тапсырыс %s тең болады %s!';
$_['error_required'] = '%s қажет!';
$_['error_product'] = 'сіздің себетте тауар жоқ!';
$_['error_recurring_required'] = 'Таңдаңыз төлем профилі!';
