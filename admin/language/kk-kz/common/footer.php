<?php
// Text
$_['text_information'] = 'Ақпарат';
$_['text_service'] = 'Колдау кызметі';
$_['text_extra'] = 'Қосымша';
$_['text_contact'] = 'Бізбен';
$_['text_return'] = 'Тауарды Қайтару';
$_['text_sitemap'] = 'Сайт картасы';
$_['text_manufacturer'] = 'Өндірушілер';
$_['text_voucher'] = 'Сыйлық сертификаттар';
$_['text_affiliate'] = 'Серіктестер';
$_['text_special'] = 'Тауарлар жеңілдікпен';
$_['text_account'] = 'Жеке кабинетіне';
$_['text_order'] = 'Тарих тапсырыстарды';
$_['text_wishlist'] = 'Менің отырғызу';
$_['text_newsletter'] = 'Таспасы Жіберу';
$_['text_powered'] = 'Жұмыс Істейді <a target="_blank" href="http://myopencart.com/">ocStore</a><br / > %s &copy; %s <br />Сайтты әзірлеу <a target="_blank" href="https://schegloff.kz/">schegloff.kz</a>';