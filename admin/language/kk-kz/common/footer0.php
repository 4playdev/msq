<?php
// Text
$_['text_information'] = 'Ақпарат';
$_['text_service'] = 'қолдау Қызметі';
$_['text_extra'] = 'Қосымша';
$_['text_contact'] = 'бізбен';
$_['text_return'] = 'тауарды Қайтару';
$_['text_sitemap'] = 'Сайт картасы';
$_['text_manufacturer'] = 'Өндірушілер';
$_['text_voucher'] = 'Сыйлық сертификаттар';
$_['text_affiliate'] = 'Серіктестер';
$_['text_special'] = ', Тауарлар жеңілдікпен';
$_['text_account'] = 'Жеке кабинетіне';
$_['text_order'] = 'Тарих тапсырыстарды';
$_['text_wishlist'] = 'Менің отырғызу';
$_['text_newsletter'] = 'таспасы Жіберу';
$_['text_powered'] = 'жұмыс Істейді <мақсатқа="_blank" href="http://myopencart.com/">ocStore</а><br / > %s &көшірмесі; %ы';