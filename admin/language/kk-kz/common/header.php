<?php
// Text
$_['text_home'] = 'Басты бет';
$_['text_wishlist'] = 'Менің отырғызу (%s)';
$_['text_shopping_cart'] = 'Себет сатып алу';
$_['text_category'] = 'Санат';
$_['text_account'] = 'Жеке кабинетіне';
$_['text_register'] = 'Тіркеу';
$_['text_login'] = 'Авторизациялау';
$_['text_order'] = 'Тарих тапсырыстарды';
$_['text_transaction'] = 'Тарих төлемдер';
$_['text_download'] = 'Файлдарды жүктеу';
$_['text_logout'] = 'Шығу';
$_['text_checkout'] = 'Тапсырысты ресімдеу';
$_['text_search'] = 'Іздеу';
$_['text_all'] = 'Барлық көрсету';
$_['text_page'] = 'Бет';
