<?php
// Heading
$_['heading_title'] = 'Жеке кабинетіне';

// Text
$_['text_register'] = 'Тіркеу';
$_['text_login'] = 'Авторизациялау';
$_['text_logout'] = 'Шығу';
$_['text_forgotten'] = 'Еске салу пароль';
$_['text_account'] = 'Жеке кабинетіне';
$_['text_edit'] = 'Тіркелгі';
$_['text_password'] = 'Парольді Ауыстыру';
$_['text_address'] = 'Жеткізу Мекен-жайы';
$_['text_wishlist'] = 'Мои закладки';
$_['text_order'] = 'Тарих тапсырыстарды';
$_['text_download'] = 'Файлдарды жүктеу';
$_['text_reward'] = 'Бонустық баллдар';
$_['text_return'] = 'тауарды Қайтару';
$_['text_transaction'] = 'Тарих төлемдер';
$_['text_newsletter'] = 'Жаңалықтарға Жазылу';
$_['text_recurring'] = 'Ағымдық төлемдер';