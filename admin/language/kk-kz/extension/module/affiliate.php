<?php
// Heading
$_['heading_title'] = 'Кабинетінің серіктес';

// Text
$_['text_register'] = 'Тіркеу';
$_['text_login'] = 'Авторизациялау';
$_['text_logout'] = 'Шығу';
$_['text_forgotten'] = 'Еске салу пароль';
$_['text_account'] = 'Жеке кабинетіне';
$_['text_edit'] = 'Мәліметтер есептік жазбасын';
$_['text_password'] = 'Парольді Ауыстыру';
$_['text_payment'] = 'Төлеу Тәсілі';
$_['text_tracking'] = 'Рефералы серіктес';
$_['text_transaction'] = 'Тарих төлемдерді';