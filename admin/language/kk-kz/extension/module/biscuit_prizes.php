<?php
    
    $_['heading_title']  = 'Регистрация в розыгрыше';
    
    $_['text_extension'] = 'Расширения';
    $_['text_success']   = 'Настройки успешно изменены!';
    $_['text_edit']      = 'Настройки модуля';
    $_['entry_status']   = 'Статус';
    $_['text_enabled']   = 'Включено';
    $_['text_disabled']  = 'Выключено';
    
    // Error
    $_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';
