<?php
// Heading
$_['heading_title'] = 'Фильтр тауарларды';

// Text
$_['text_select_value'] = '-- --Таңдаңыз';
$_['text_manufacturer'] = 'Өндірушілер';
$_['text_prices'] = 'Баға ';

$_['text_empty_1'] = 'Іздеу орындалды сәтті бірақ нәтиже жоқ.';
$_['text_empty_2'] = 'Жоқ тауарлар үшін іздеу.';
$_['text_selected_filters'] = 'Сіз қолданды';

$_['button_filter'] = 'Табу';
$_['button_reset'] = 'Ағызу';
$_['button_popup_view'] = 'Жылдам қарау';
$_['button_cart'] = 'себетке';