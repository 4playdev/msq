<?php

// Text
$_['text_logout'] = 'Шығу';
$_['text_register'] = 'Тіркеу';
$_['text_forgotten'] = 'парольді Ұмыттыңыз ба?';
$_['text_category'] = 'тізімі Санаттар';
$_['text_all_category'] = 'барлық';
$_['text_menu'] = 'Мәзір';
$_['text_back'] = 'Артқа';
$_['text_info'] = 'Ақпарат';
$_['text_acc'] = 'Жеке кабинетіне';
$_['text_contacts'] = 'Байланыс';
$_['text_settings'] = 'Параметрлер';

// Button
$_['button_login'] = 'Кіру';

// Entry
$_['entry_email'] = 'E-Mail';
$_['entry_password'] = 'Пароль';