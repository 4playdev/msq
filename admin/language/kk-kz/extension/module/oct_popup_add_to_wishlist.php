<?php
// Heading
$_['heading_title_ok'] = 'өте Жақсы!';
$_['heading_title_no'] = 'Назар аударыңыз!';

// Button
$_['button_ok'] = 'Ok';

// Text
$_['text_success_add'] = 'Сіз қосылды';
$_['text_wishlist'] = 'бетбелгілер!';
$_['text_warning_add'] = 'кіру Қажет <a href="%s">Жеке Кабинетіне</a> немесе <a href="%s">тіркелгі</a> қосу үшін тауар <a href="%s">%s</a> өз <a href="%s">бетбелгілер</a>!';