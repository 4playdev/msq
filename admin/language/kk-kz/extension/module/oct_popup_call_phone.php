<?php
// Heading
$_['heading_title'] = 'Қайта қоңырау шалу';

// Button
$_['button_close'] = 'Жабу';
$_['button_send'] = 'Жіберу';

// Text
$_['text_success_send'] = '<p>Сіздің сұрауыңыз сәтті жіберілді!</p>';
$_['text_loading'] = 'Жүктеу...';
$_['call_back'] = 'Сіз, біз Сізге қоңырау шаламыз?';
$_['text_info'] = 'Пайдаланушы';
$_['text_date_added'] = 'Filed';

// Enter
$_['enter_name'] = 'Сіздің атыңыз Енгізіңіз';
$_['enter_telephone'] = 'Енгізіңіз телефоны';
$_['enter_time'] = 'Қашан қоңырау шалу үшін?';
$_['enter_comment'] = 'Пікір';

// Error
$_['error_name'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_time'] = 'Күн-ай/уақыт енгізілген жоқ дұрыс!';
$_['error_telephone'] = 'Телефон тиіс 3-тен 32 символдан!';
$_['error_comment'] = ', Пікір тиіс 3 500-ге дейін символ!';