<?php
// Heading
$_['heading_title'] = 'Себет';

// Text
$_['text_success'] = '<a href="%s">%s</a> қосылды <a href="%s">себетке сатып алу</a>!';
$_['text_remove'] = 'Себет сауда өзгертілді!';
$_['text_login'] = 'Қажет <a href="%s">авторизация жасай</a> немесе <a href="%s">тіркелгі</a> бағаларды қарау үшін!';
$_['text_cart_items'] = 'себетте <span class="gold">%s тауарлардың</span> сомаға <span class="gold">%s</span>';
$_['text_points'] = 'Бонустық ұпайлар: %s';
$_['text_empty'] = 'Қоржын бос!';
$_['text_day'] = '';
$_['text_week'] = 'апта';
$_['text_semi_month'] = 'мұсылмандарға';
$_['text_month'] = 'ай';
$_['text_year'] = 'жыл';
$_['text_trial'] = 'Құны: %s; Кезеңділігі: %s %s; Саны төлемдер: %s; Бұдан әрі, ';
$_['text_recurring'] = 'Құны: %s; Кезеңділігі: %s %s';
$_['text_length'] = 'Саны, төлем %s';
$_['text_until_cancelled'] = 'күшін жою';
$_['text_recurring_item'] = 'Ағымдық төлемдер';
$_['text_payment_recurring'] = 'Төлем профиль';
$_['text_trial_description'] = 'Құны: %s; Кезеңділігі: %d %s; Саны, төлем %d; Бұдан әрі, ';
$_['text_payment_description'] = 'Құны: %s; Кезеңділігі: %d %s; Саны, төлем %d';
$_['text_payment_cancel'] = 'Құны: %s; Кезеңділігі: %d %s; Саны, төлем дейін күшін жою';

$_['button_shopping'] = 'Оралу алулар';
$_['button_checkout'] = 'Сатып алу';

$_['column_image'] = 'Видео';
$_['column_name'] = 'Атауы';
$_['column_quantity'] = 'Саны';
$_['column_price'] = 'Баға';

// Error
$_['error_stock'] = 'Тауарларға белгіленген *** жоқ қажетті саны немесе олардың жоқ қоймада!';
$_['error_minimum'] = 'ең Аз саны үшін тапсырыс тауардың %s құрайды, %s!';
$_['error_required'] = '%s міндетті түрде!';
$_['error_product'] = 'сіздің себетте тауар жоқ!';
$_['error_recurring_required'] = 'таңдаңыз кезеңділігі төлем!';