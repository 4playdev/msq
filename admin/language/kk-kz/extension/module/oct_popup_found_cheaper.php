<?php
// Heading
$_['heading_title'] = 'Тауып бұл тауарды арзан?';

// Button
$_['button_close'] = 'Оралу алулар';
$_['button_send'] = 'Жіберу';

// Text
$_['text_success_send'] = '<p>Сіздің сұрауыңыз сәтті жіберілді!</p>';
$_['text_loading'] = 'Жүктеу...';

$_['text_info'] = 'Пайдаланушы';
$_['text_date_added'] = 'Filed';

// Enter
$_['enter_name'] = 'Аты';
$_['enter_telephone'] = 'Телефоны';
$_['enter_link'] = 'тауарға Сілтеме интернетте';
$_['enter_comment'] = 'Пікір';
$_['enter_referer'] = 'тауарға Сілтеме';

// Error
$_['error_name'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_time'] = 'Сілтеме енгізілген дұрыс емес!';
$_['error_telephone'] = 'Телефон тиіс 3-тен 32 символдан!';
$_['error_link'] = 'Көрсетіңіз сілтеме табылған тауарға!';
$_['error_comment'] = ', Пікір тиіс 3 500-ге дейін символ!';