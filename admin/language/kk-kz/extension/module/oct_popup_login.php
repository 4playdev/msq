<?php
// Heading
$_['heading_title'] = 'Авторизациялау';

// Text 
$_['text_login'] = 'енгізіңіз логин және пароль';

$_['button_login'] = 'Кіру';
$_['button_register'] = 'Тіркеу';
$_['button_forgotten'] = 'Забыли пароль';

// Entry
$_['entry_email'] = 'E-Mail';
$_['entry_password'] = 'Пароль';

// Error
$_['error_login'] = 'Дұрыс емес толтырылған өріс E-Mail және/немесе пароль!';
$_['error_attempts'] = 'Назар салыңыз: Сіз асып рұқсат етілген саны әрекеттерінің жүйесіне кіру. Көріңіз бір сағат.';
$_['error_approved'] = 'Қажет тіркелгіні алдында авторизацией.';