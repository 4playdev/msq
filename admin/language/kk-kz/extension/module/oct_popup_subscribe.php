<?php
$_['heading_title'] = 'қош келдіңіздер!';

// Button
$_['button_subscribe'] = 'Жазылу';
$_['button_close'] = 'сайты';

// Text
$_['text_success_subscribe'] = '<p>Сіз сәтті жасады подписку!</p><p>Сіздің E-mail хат жіберілген сілтеме жасай отырып жазылуды растау.</p>';
$_['text_stop'] = 'көрсете бұл терезе келесі жолы.';
$_['text_thanks'] = 'Рахмет';

$_['text_approve_welcome'] = 'Қолдаймыз және благодарим за подписку %s!';
$_['text_approve_services'] = 'жауап бермеңіз, бұл хатты сіз ол келді сізге қате.';
$_['text_approve_subject'] = '%s Растау жазылу!';
$_['text_subscribe_services'] = 'растау Үшін жазылу керек сілтемесі бойынша өту: %s';

$_['text_unsubscribe_welcome'] = 'Қолдаймыз және благодарим за подписку %s!';
$_['text_unsubscribe_subject'] = '%s - Сіздің жазылу сәтті расталды!';
$_['text_unsubscribe_services'] = 'Сіз отменить подписку егер өтесіз, сілтеме: %s';

// Enter
$_['enter_email'] = 'Енгізіңіз Email алыңыз сыйлық!';

// Error
$_['error_enter_email'] = 'Енгізіңіз Email!';
$_['error_valid_email'] = 'Email енгізілді дұрыс емес!';
$_['error_already_subscribed_email'] = 'Бұл e-Mail қазірдің өзінде қол қойылды!';