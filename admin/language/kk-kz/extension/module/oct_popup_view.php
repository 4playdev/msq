<?php
// Heading
$_['heading_title'] = ', тауар Туралы';

// Button
$_['button_oct_popup_view'] = 'Жылдам қарау';
$_['button_view'] = 'Толығырақ';
$_['button_continue'] = 'пікір Қалдыру';
$_['button_cart'] = 'себетке';
$_['button_compare'] = 'бұтақ кескіш';
$_['button_wishlist'] = 'тізіміне тілек';
$_['text_oct_popup_purchase'] = 'Тапсырыс бір клик';
$_['text_oct_popup_found_cheaper'] = 'Таптық арзан?';
$_['button_close'] = 'Оралу алулар';

// Text
$_['text_price'] = 'Баға:';
$_['text_manufacturer'] = 'Бренді:';
$_['text_model'] = 'Моделі:';
$_['text_reward'] = 'Бонустық балдар:';
$_['text_points'] = 'Баға бонустық балах:';
$_['text_stock'] = 'Болуы:';
$_['text_instock'] = 'бар';
$_['text_tax'] = 'Без налога:';
$_['text_discount'] = ' немесе ';
$_['text_option'] = 'Қолжетімді нұсқалары';
$_['text_minimum'] = 'ең Аз саны үшін тауарды тапсырыс %s';
$_['text_reviews'] = '%s пікір';
$_['text_write'] = 'пікір Жазу';
$_['text_login'] = 'Пожалуйста <a href="%s">кіру</a> немесе <a href="%s">тіркеліңіз</a> жазу үшін кері қайтарып алу';
$_['text_no_reviews'] = 'тауар туралы Пікірлер әзірше жоқ.';
$_['text_note'] = '<span class="text-danger">Назар аударыңыз:</span> HTML теги қолдау көрсетілмейді!';
$_['text_success'] = 'Рахмет пікіріңіз үшін. Жарияланым пайда болады тексергеннен кейін әкімшісі.';
$_['text_tags'] = 'Теги:';
$_['text_error'] = 'Тауар табылмады!';
$_['text_payment_recurring'] = 'Төлем профиль';
$_['text_trial_description'] = 'Құны: %s; Кезеңділігі: %d %s; Саны, төлем %d; Бұдан әрі, ';
$_['text_payment_description'] = 'Құны: %s; Кезеңділігі: %d %s; Саны, төлем %d';
$_['text_payment_cancel'] = 'Құны: %s; Кезеңділігі: %d %s; Саны, төлем дейін күшін жою';
$_['text_day'] = '';
$_['text_week'] = 'апта';
$_['text_semi_month'] = 'жарты';
$_['text_month'] = 'ай';
$_['text_year'] = 'жыл';
$_['text_select'] = ' --- Тізімнен таңдау --- ';
$_['text_loading'] = 'Жүктеу...';

// Enter
$_['entry_qty'] = 'Саны';
$_['entry_name'] = 'аты';
$_['entry_review'] = 'Сіздің пікір';
$_['entry_rating'] = 'Баға';
$_['entry_good'] = 'Жақсы';
$_['entry_bad'] = 'Жаман';

// Tabs
$_['tab_description'] = 'Сипаттамасы';
$_['tab_attribute'] = 'Сипаттамасы';
$_['tab_review'] = 'Пікірлер (%s)';

// Error
$_['error_option'] = '%s міндетті!';
$_['error_name'] = 'Назар аударыңыз: Аты болуы тиіс 3-тен 25 символдан!';
$_['error_text'] = 'Назар аударыңыз: Пікір тиіс 25-1000 символдармен!';
$_['error_rating'] = 'Назар аударыңыз: Көрсетіңіз, өз бағасын!';
$_['error_captcha'] = 'Назар аударыңыз: тексеру Кодын келмесе, с картинкой!';