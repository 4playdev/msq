<?php
// Heading
$_['heading_title'] = 'алдын ала';

// Button
$_['button_close'] = 'Жабу';
$_['button_send'] = 'Жіберу';

// Text
$_['text_success_send'] = '<p>Сіздің сұрауыңыз сәтті жіберілді!</p>';
$_['text_loading'] = 'Жүктеу...';

$_['text_info'] = 'Пайдаланушы';
$_['text_date_added'] = 'Filed';
$_['text_out_of_stock'] = 'Бітті';

// Enter
$_['enter_name'] = 'Аты';
$_['enter_telephone'] = 'Телефоны';
$_['enter_email'] = 'Email';
$_['enter_comment'] = 'Пікір';
$_['enter_referer'] = 'тауарға Сілтеме';

// Error
$_['error_name'] = 'Аты болуы тиіс 1-ден 32-ге рәміздер!';
$_['error_email'] = 'Email міндетті түрде толтыру!';
$_['error_valid_email'] = 'Email енгізілді дұрыс емес!';
$_['error_telephone'] = 'Телефон тиіс 3-тен 32 символдан!';
$_['error_comment'] = ', Пікір тиіс 3 500-ге дейін символ!';