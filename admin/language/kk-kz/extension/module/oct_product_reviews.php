<?php

// Text
$_['entry_positive_text'] = 'Артықшылықтары:';
$_['entry_negative_text'] = 'Кемшіліктері:';
$_['text_where_bought'] = 'Шығарылғанын осы дүкенде:';
$_['text_admin_answer'] = 'Жауап әкімшісі:';
$_['text_where_bought_yes'] = 'Иә';
$_['text_where_bought_no'] = 'Жоқ';
$_['text_my_assessment'] = 'Менің бағалау:';

// Success
$_['text_success'] = 'Сіз сәтті дауыс!';

// Error
$_['error_ip_exist'] = 'Қате: Сіз дауыс берді!';