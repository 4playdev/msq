<?php
// Heading
$_['heading_title'] = 'Жазылу';

// Button
$_['button_subscribe'] = 'Жазылу';

// Enter
$_['enter_email'] = 'Енгізіңіз Email';

// Error
$_['error_enter_email'] = 'Енгізіңіз Email!';
$_['error_valid_email'] = 'Email енгізілді дұрыс емес!';
$_['error_already_subscribed_email'] = 'Бұл e-Mail қазірдің өзінде қол қойылды!';