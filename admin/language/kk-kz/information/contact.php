<?php
// Heading
$_['heading_title'] = 'Бізбен';

// Text
$_['text_location'] = 'Мекен жайы';
$_['text_store'] = 'Біздің дүкендер';
$_['text_contact'] = 'Нысаны байланыс';
$_['text_address'] = 'Мекен жайы';
$_['text_telephone'] = 'Телефоны';
$_['text_fax'] = 'Факс';
$_['text_open'] = 'Жұмыс';
$_['text_comment'] = 'Пікір';
$_['text_success'] = '<p>Сіздің сұрауыңыз сәтті жіберілді әкімшілігінің дүкенінің!</p>';

// Entry
$_['entry_name'] = 'Аты';
$_['entry_email'] = 'E-Mail байланыс үшін';
$_['entry_enquiry'] = 'Хабар';

// E-mail
$_['email_subject'] = 'Хабар %s';

// Errors
$_['error_name'] = 'Аты болуы тиіс 3-тен 32 символдан!';
$_['error_email'] = ', E-Mail көрсетілуі дұрыс емес!';
$_['error_enquiry'] = 'Хабарлама болуы тиіс 10-нан 3000 символдан!';