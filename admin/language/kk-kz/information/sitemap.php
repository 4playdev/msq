<?php
// Heading
$_['heading_title'] = 'Сайт картасы';

// Text
$_['text_special'] = 'Тауарлар жеңілдікпен';
$_['text_account'] = 'Жеке кабинетіне';
$_['text_edit'] = 'тіркелгі';
$_['text_password'] = 'Парольді ауыстыру';
$_['text_address'] = 'Мекенжайларының Тізімі жеткізуді';
$_['text_history'] = 'Тарих тапсырыстарды';
$_['text_download'] = 'Файлдарды жүктеу';
$_['text_cart'] = 'Себет сатып алу';
$_['text_checkout'] = 'Тапсырысты Ресімдеу';
$_['text_search'] = 'Іздеу';
$_['text_information'] = 'Ақпарат';
$_['text_contact'] = 'Бізбен';
