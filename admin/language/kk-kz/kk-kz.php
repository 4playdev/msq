<?php
// Locale
$_['code']                  = 'kk';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd.m.Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ' ';

// Text
$_['text_home'] = '<i class="fa fa-home"></i>';
$_['text_yes'] = 'Иә';
$_['text_no'] = 'Жоқ';
$_['text_none'] = ' --- таңдалған Жоқ --- ';
$_['text_select'] = ' --- Таңдаңыз --- ';
$_['text_all_zones'] = 'Барлық аймақтар';
$_['text_pagination'] = 'Көрсетілген %d %d %d, барлығы %d беттер)';
$_['text_loading'] = 'Жүктеу...';
$_['text_no_results'] = 'нәтижелер Жоқ!';

// Buttons
$_['button_address_add'] = 'мекенжайын Қосу';
$_['button_back'] = 'Артқа';
$_['button_continue'] = 'Жалғастыру';
$_['button_cart'] = 'себетке';
$_['button_cancel'] = 'Болдырмау';
$_['button_compare'] = 'салыстыру';
$_['button_wishlist'] = 'қалау';
$_['button_checkout'] = 'тапсырысты Ресімдеу';
$_['button_confirm'] = 'тапсырысты';
$_['button_coupon'] = 'Қолдануға купон';
$_['button_delete'] = 'Жою';
$_['button_download'] = 'Жүктеу';
$_['button_edit'] = 'Өңдеу';
$_['button_filter'] = 'Нақтылау іздеу';
$_['button_new_address'] = 'Жаңа мекен-жайы';
$_['button_change_address'] = 'мекенжайын Өзгерту';
$_['button_reviews'] = 'Пікірлер';
$_['button_write'] = 'пікір Жазу';
$_['button_login'] = 'Кіру';
$_['button_update'] = 'Жаңарту';
$_['button_remove'] = 'Жою';
$_['button_reorder'] = 'Қосымша тапсырыс бойынша';
$_['button_return'] = 'тауарды Қайтару';
$_['button_shopping'] = 'Жалғастыру сатып алу';
$_['button_search'] = 'Іздеу';
$_['button_shipping'] = 'Қолдануға жеткізуді';
$_['button_submit'] = 'Жіберу';
$_['button_guest'] = 'тапсырысты Ресімдеу без регистрации';
$_['button_view'] = 'Қарап шығу';
$_['button_voucher'] = 'Қолдануға сертификаты';
$_['button_upload'] = 'файлды Жүктеу';
$_['button_reward'] = 'Қолдануға бонустық баллдар';
$_['button_quote'] = 'Сіз quotes';
$_['button_list'] = 'Тізімін';
$_['button_grid'] = 'Кесте';
$_['button_map'] = 'Қарасақ Google картасында';

// Error
$_['error_exception'] = 'қате Коды (%s): %s %s-жолда %s';
$_['error_upload_1'] = 'Назар аударыңыз: Мөлшері жүктелетін файлдың асатын мәні директивасының upload_max_filesize php.ini!';
$_['error_upload_2'] = 'Назар аударыңыз: Мөлшері жүктелетін файлдың асатын мәні директивасының MAX_FILE_SIZE көрсетілген нысан HTML!';
$_['error_upload_3'] = 'Ескерту: Жүктелген файл тие-жартылай ғана!';
$_['error_upload_4'] = 'Назар аударыңыз: Файл емес жүктелді!';
$_['error_upload_6'] = 'Ескерту: уақытша қалта табылмады!';
$_['error_upload_7'] = 'Ескерту: Мүмкін болмады файлды дискіге жазу!';
$_['error_upload_8'] = 'Ескерту: файл Жүктеу тоқтатылды кеңейтумен!';
$_['error_upload_999'] = 'Ескерту: қате Коды қол жетімді емес!';
$_['error_curl'] = 'CURL: қате Коды(%s) %s';
