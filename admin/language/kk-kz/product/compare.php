<?php
// Heading
$_['heading_title'] = 'Салыстыру тауарлар';

// Text
$_['text_product'] = 'тауар туралы Ақпарат';
$_['text_name'] = 'Атауы';
$_['text_image'] = 'Сурет';
$_['text_price'] = 'Баға';
$_['text_model'] = 'Үлгі';
$_['text_manufacturer'] = 'Жүргізуші';
$_['text_availability'] = 'Қолжетімділігі';
$_['text_instock'] = 'Бар';
$_['text_rating'] = 'Рейтингі';
$_['text_reviews'] = 'Негізінде' %s ' кері қайтарып алу(дар).';
$_['text_summary'] = 'Түйіндеме';
$_['text_weight'] = 'Салмағы';
$_['text_dimension'] = '(Д х Ш х В)';
$_['text_compare'] = 'Салыстыру тауарларды (%s)';
$_['text_success'] = 'Тауарға <a href="%s">%s</a> сәтті қосылды <a href="%s">салыстыру</a>!';
$_['text_remove'] = 'Тауарлардың тізімі сәтті жаңартылды!';
$_['text_empty'] = 'Сіз таңдаған жоқ бірде-бір тауар салыстыру үшін.';