<?php
// Heading
$_['heading_title'] = 'Өндірушілердің тізімі';

// Text
$_['text_brand'] = 'Өндірушілер';
$_['text_index'] = 'Алфавиттік көрсеткіші:';
$_['text_error'] = 'Өндіруші табылмады!';
$_['text_empty'] = 'Тауар жоқ';
$_['text_quantity'] = 'Саны:';
$_['text_manufacturer'] = ') Өндіруші:';
$_['text_model'] = 'Тауардың коды:';
$_['text_points'] = 'Бонустық ұпайлар:';
$_['text_price'] = 'Баға:';
$_['text_tax'] = 'Салықсыз:';
$_['text_compare'] = 'Салыстыру тауарларды (%s)';
$_['text_sort'] = 'Сұрыптау:';
$_['text_default'] = 'әдепкі';
$_['text_name_asc'] = '(A)';
$_['text_name_desc'] = 'Аты бойынша (Я - A)';
$_['text_price_asc'] = 'Баға бойынша (өсуі)';
$_['text_price_desc'] = 'Баға бойынша (кемуі)';
$_['text_rating_asc'] = 'Рейтингі бойынша (өсуі)';
$_['text_rating_desc'] = 'Рейтингі бойынша (кемуі)';
$_['text_model_asc'] = 'Моделі бойынша (A - Z)';
$_['text_model_desc'] = 'Моделі бойынша (Z - A)';
$_['text_limit'] = 'Көрсету:';