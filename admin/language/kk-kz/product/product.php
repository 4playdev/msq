<?php
// Text
$_['text_search'] = 'Іздеу';
$_['text_brand'] = 'Жүргізуші';
$_['text_manufacturer'] = ') Өндіруші:';
$_['text_model'] = 'Тауардың Коды:';
$_['text_reward'] = 'Бонустық ұпайлар:';
$_['text_points'] = 'Баға бонустық ұпайлардың:';
$_['text_stock'] = 'Қолжетімділігі:';
$_['text_instock'] = 'Коймада';
$_['text_tax'] = ', ҚҚС-Сыз:';
$_['text_discount'] = 'Немесе ';
$_['text_option'] = 'Кол Жетімді опциялар';
$_['text_minimum'] = 'Ең Аз саны тапсырыс беру үшін бұл тауардың: %s. ';
$_['text_reviews'] = '%s пікір';
$_['text_write'] = 'Пікір Жазу';
$_['text_login'] = 'Пожалуйста <a href="%s"> авторизациялаудан өту</a> немесе <a href="%s"> тіркеліңіз</a> қарап шығу';
$_['text_no_reviews'] = 'Жоқ пікірлер бұл тауар.';
$_['text_note'] = '<span class="text-danger">Назар аударыңыз:</span> HTML қолдау көрсетілмейді! Пайдаланыңыз қарапайым мәтін!';
$_['text_success'] = 'Рахмет пікіріңіз үшін. Ол бағытталған модерацию.';
$_['text_related'] = 'Ұсынылатын тауарлар';
$_['text_tags'] = 'Теги:';
$_['text_error'] = 'Тауар табылмады!';
$_['text_payment_recurring'] = 'Төлем профильдер';
$_['text_trial_description']                  = '%s каждый %d %sй для %d оплат(ы),';
$_['text_payment_description']                = '%s каждый %d %s(-и) из %d платежей(-а)';
$_['text_payment_cancel']                     = '%s every %d %s(s) until canceled';
$_['text_day'] = '';
$_['text_week'] = 'Апта';
$_['text_semi_month'] = 'Мұсылмандарға';
$_['text_month'] = 'Ай';
$_['text_year'] = 'Жыл';

// Entry
$_['entry_qty'] = 'Саны';
$_['entry_name'] = 'Сіздің атыңыз:';
$_['entry_review'] = 'Сіздің пікір';
$_['entry_rating'] = 'Рейтингі';
$_['entry_good'] = 'Жақсы';
$_['entry_bad'] = 'Жаман';

// Tabs
$_['tab_description'] = 'Сипаттамасы';
$_['tab_attribute'] = 'Сипаттамалары';
$_['tab_review'] = 'Пікірлер (%s)';

// Error
$_['error_name'] = 'Аты болуы тиіс 3-тен 25 символдан!';
$_['error_text'] = 'Мәтін қайтарып алу керек 25-тен 1000 таңба!';
$_['error_rating'] = 'Пожалуйста қойыңыз бағалауды!';
