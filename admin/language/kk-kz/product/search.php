<?php
// Heading
$_['heading_title'] = 'Іздеу';
$_['heading_tag'] = 'Тег - ';

// Text
$_['text_search'] = 'Тауарлар, тиісті іздеу';
$_['text_keyword'] = 'tags';
$_['text_category'] = 'Барлық санаттар';
$_['text_sub_category'] = 'Іздеу подкатегориях';
$_['text_empty'] = 'Жоқ тауарларды, тиісті іздеу.';
$_['text_quantity'] = 'Саны:';
$_['text_manufacturer'] = ') Өндіруші:';
$_['text_model'] = 'Тауардың Коды:';
$_['text_points'] = 'Бонустық ұпайлар:';
$_['text_price'] = 'Баға:';
$_['text_tax'] = ', ҚҚС-Сыз:';
$_['text_reviews'] = 'Негізінде' '%s' ' пікірлер.';
$_['text_compare'] = 'Салыстыру тауарларды (%s)';
$_['text_sort'] = 'Сұрыптау:';
$_['text_default'] = 'әдепкі';
$_['text_name_asc'] = 'Аты Бойынша ''(А-Я)';
$_['text_name_desc'] = 'Аты Бойынша ''(Я - А) -';
$_['text_price_asc'] = 'Баға Бойынша' 'Tөсуі)';
$_['text_price_desc'] = 'Баға Бойынша' '(Кемуі)';
$_['text_rating_asc'] = 'Рейтингі Бойынша' '(Кемуі)';
$_['text_rating_desc'] = 'Рейтингі Бойынша' '(Есуі)';
$_['text_model_asc'] = 'Моделі Бойынша' '(А - Я)';
$_['text_model_desc'] = 'Моделі Бойынша' '(Я - А) -';
$_['text_limit'] = 'Көрсету:';

// Entry
$_['entry_search'] = 'Iздестіру Критерийлері';
$_['entry_description'] = 'Искать сипаттамада тауардың';