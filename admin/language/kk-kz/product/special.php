<?php
// Heading
$_['heading_title'] = 'Тауарлар жеңілдікпен';

// Text
$_['text_empty'] = 'Казіргі уақытта тауарлар жеңілдікпен қол жетімді емес';
$_['text_quantity'] = 'Саны:';
$_['text_manufacturer'] = ') Өндіруші:';
$_['text_model'] = 'Тауардың Коды:';
$_['text_points'] = 'Бонустық ұпайлар:';
$_['text_price'] = 'Баға:';
$_['text_tax'] = ', ҚҚС-Сыз:';
$_['text_compare'] = 'Салыстыру тауарларды (%s)';
$_['text_sort'] = 'Сұрыптау:';
$_['text_default'] = 'әдепкі';
$_['text_name_asc'] = 'Аты Бойынша (А-Я)';
$_['text_name_desc'] = 'Аты Бойынша (Я - А) -';
$_['text_price_asc'] = 'Баға Бойынша (өсуі)';
$_['text_price_desc'] = 'Баға Бойынша (кемуі)';
$_['text_rating_asc'] = 'Рейтингі Бойынша (кемуі)';
$_['text_rating_desc'] = 'Рейтингі Бойынша (өсуі)';
$_['text_model_asc'] = 'Моделі Бойынша (А - Я)';
$_['text_model_desc'] = 'Моделі Бойынша (Я - А) -';
$_['text_limit'] = 'Көрсету:';