<?php
    
$_['heading_title']  = 'MSQ Карточки';
    
// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_title']      = 'Заголовок';
$_['entry_bg_color']   = 'Цвет фона';
$_['entry_link']       = 'Ссылка ';
$_['entry_type']       = 'Тип карточки';

$_['entry_banner_1']     = 'Баннер 1';
$_['entry_banner_2']     = 'Баннер 2';
$_['entry_banner_3']     = 'Баннер 3';

$_['entry_banner_type_1']     = 'Тип 1';
$_['entry_banner_type_2']     = 'Тип 2';
$_['entry_banner_type_3']     = 'Тип 3';

$_['entry_title_1']       = 'Текст 1';
$_['entry_title_2']       = 'Текст 2';
$_['entry_title_3']       = 'Текст 3';




$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';
$_['error_name']       = 'Название должно содержать от 3 до 64 символов!';
$_['error_title']      = 'Введите заголовок';
$_['error_banner']     = 'Выберите баннер';
$_['error_link']       = 'Введите ссылку';
