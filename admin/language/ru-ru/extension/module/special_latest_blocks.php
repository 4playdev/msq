<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Блоки Акция и Новинки';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

// Entry
$_['entry_name']            = 'Название модуля';
$_['entry_special_product'] = 'Продукт Акция';
$_['entry_latest_product']  = 'Продукт Новинка';
$_['entry_width']           = 'Ширина';
$_['entry_height']          = 'Высота';
$_['entry_status']          = 'Статус';

// Help
$_['help_product']     = '(Автозаполнение)';

// Error
$_['error_permission']      = 'У вас недостаточно прав для внесения изменений!';
$_['error_name']            = 'Название должно содержать от 3 до 64 символов!';
$_['error_special_product'] = 'Выберите продукт Акция!';
$_['error_latest_product']  = 'Выберите продукт Новинка!';
$_['error_width']           = 'Укажите Ширину!';
$_['error_height']          = 'Укажите Высоту!';