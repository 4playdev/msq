<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerAccountJoin extends Controller {
	public function index() {
		if ($this->customer->isLogged())
			$this->response->redirect($this->url->link('account/login', '', true));

		$url = isset($this->request->get['email'])
			? "email={$this->request->get['email']}"
			: "";

		if (!isset($this->request->get['type']) || $this->request->get['type'] === 'register')
			$this->response->redirect($this->url->link('account/register', $url, true));

		if ($this->request->get['type'] === 'login')
			$this->response->redirect($this->url->link('account/login', $url, true));

		$this->response->redirect($this->url->link('/'));
	}
}
