<?php
class ControllerApiProduct extends Controller
{
    public function getProducts()
    {
		$this->load->language('api/cart');

		$json = array();
			
        if (!isset($this->session->data['api_id'])) {
            $json['error']['warning'] = $this->language->get('error_permission');
        } else {
			$this->load->model('api/product');
			$json['products'] = array();
			$filter_data = array();
			$results = $this->model_api_product->getProductsAPI($filter_data);
			
			foreach ($results as $result) {
				array_push($json['products'], $result);
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
    }
	
	public function updateProducts()
    {
		$this->load->language('api/cart');

		$json = array();
			
        if (!isset($this->session->data['api_id'])) {
            $json['error']['warning'] = $this->language->get('error_permission');
        } else {
			$this->load->model('api/product');
			$products = json_decode(file_get_contents('php://input'), true);
			$json['products'] = $products['products'];
			
			foreach ($json['products'] as $product) {
				$this->model_api_product->editProduct($product['product_id'], $product);
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
    }
}
