	<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		// foreach ($this->model_catalog_information->getInformations() as $result) {
			// if ($result['bottom']) {
				// $data['informations'][] = array(
					// 'title' => $result['title'],
					// 'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				// );
			// }
		// }

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
//		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['lotery'] = $this->url->link('extension/module/biscuit_prizes', ''. true);
		$data['lang'] = $this->language->get('code');

		// $data['informations'][] = [
			// 'title' => $this->language->get('text_about_us'),
			// 'href'  => $this->url->link('information/information', 'information_id=4')
		// ];
		
		$data['informations'][] = [
			'title' => $this->language->get('text_lotery'),
			'href'  => $this->url->link('information/information', 'information_id=10')
		];
		
		$data['informations'][] = [
			'title' => $this->language->get('text_delivery'),
			'href'  => $this->url->link('information/information', 'information_id=6')
		];
		
		$data['informations'][] = [
			'title' => $this->language->get('text_addresses'),
			'href'  => $this->url->link('information/map')
		];
		
		$data['informations'][] = [
			'title' => $this->language->get('text_contact'),
			'href'  => $data['contact']
		];
		
		

		// $data['questions'][] = [
			// 'title' => $this->language->get('text_question_order'),
			// 'href'  => $this->url->link('information/instruction')
		// ];
		// $data['questions'][] = [
			// 'title' => $this->language->get('text_question_payment_online'),
			// 'href'  => $this->url->link('information/payment')
		// ];
		$data['questions'][] = [
			'title' => $this->language->get('text_delivery'),
			'href'  => $this->url->link('information/information', 'information_id=6')
		];
		
		
		
		$data['account'][] = [
			'title' => $this->language->get('text_my_account'),
			'href'  => $this->url->link('account/account', '', true)
		];
		$data['account'][] = [
			'title' => $this->language->get('text_setting'),
			'href'  => $this->url->link('account/edit', '', true)
		];
		$data['account'][] = [
			'title' => $this->language->get('text_wishlist'),
			'href'  => $this->url->link('account/wishlist', '', true)
		];
		// $data['account'][] = [
			// 'title' => $this->language->get('text_special'),
			// 'href'  => $this->url->link('product/special', '', true)
		// ];
		// $data['account'][] = [
			// 'title' => $this->language->get('text_reward'),
			// 'href'  => $this->url->link('account/reward', '', true)
		// ];

		// $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		$data['scripts'] = $this->document->getScripts('footer');

		// if (is_file(DIR_IMAGE . $this->config->get('config_logo_alternative'))) {
			// $data['logo'] = $server . 'image/' . $this->config->get('config_logo_alternative');
		// } else {
			// $data['logo'] = '';
		// }
		
		if ($data['lang'] == 'ru') {
			$data['logo'] = $server . 'image/'.('ru-ru/logo_ru_light_sm.png');
		} elseif ($data['lang'] == 'kk') {
			$data['logo'] = $server . 'image/'.('kk-kz/logo_kz_light_sm.png');
		} else
			$data['logo'] = $server . 'image/'.('en-gb/logo_en_light_sm.png');

		$data['telephone'] = $this->config->get('config_telephone');
		// $data['address'] = $this->config->get('config_address');

		$data['instagram'] = $this->config->get('config_instagram');
		$data['twitter'] = $this->config->get('config_twitter');
		$data['vkontakte'] = $this->config->get('config_vkontakte');
		

		$data['year'] = date('Y');

		return $this->load->view('common/footer', $data);
	}
}
