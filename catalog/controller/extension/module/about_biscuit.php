<?php
class ControllerExtensionModuleAboutBiscuit extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/about_biscuit');

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		if (isset($setting['text'][$this->config->get('config_language_id')]))
			$data['text'] = html_entity_decode($setting['text'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		else
			$data['text'] = null;

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		return $this->load->view('extension/module/about_biscuit', $data);
	}
}