<?php
class ControllerExtensionModuleAskQuestion extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/ask_question');

		$data = [];

		return $this->load->view('extension/module/ask_question', $data);
	}

	public function send(){
		$this->load->language('extension/module/ask_question');

		$result = [
			'status' => 'success'
		];

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if($this->validate()){
				$mail = new Mail($this->config->get('config_mail_engine'));
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = $this->config->get('config_mail_smtp_password');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$message = "
					<p><b>Новый вопрос</b></p>
					<p><b>Имя: </b>{$this->request->post['name']}</p>
					<p><b>E-Mail: </b>{$this->request->post['email']}</p>
					<p><b>Вопрос: </b>{$this->request->post['question']}</p>
				";

				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_mail_smtp_username'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject('Новый вопрос');
				$mail->setHtml($message);
				$mail->send();
			}else{
				$result['status'] = 'validate';
				$result['errors'] = $this->error;
			}

		}else
			$result['status'] = 'error';

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	protected function validate() {
		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['question']) < 10) || (utf8_strlen($this->request->post['question']) > 3000)) {
			$this->error['question'] = $this->language->get('error_question');
		}

		return !$this->error;
	}
}