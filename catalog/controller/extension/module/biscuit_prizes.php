<?php
    
class ControllerExtensionModuleBiscuitPrizes extends Controller {
    private $error = array();
    
    public function index() {
        // Загружаем "модель"
        $this->load->model('extension/module/biscuit_prizes');
        $data = array();
        // Загружаем настройки (для проверки включен модуль или нет)
        $data['module_biscuit_prizes_status'] = $this->model_extension_module_biscuit_prizes->LoadSettings();
        // Загружаем языковой файл
        $data += $this->load->language('extension/module/biscuit_prizes');
        // Хлебные крошки
        $data['breadcrumbs'][] = array(
                                       'text' => $this->language->get('text_home'),
                                       'href' => $this->url->link('common/home')
                                       );
        $data['breadcrumbs'][] = array(
                                       'text' => $data['heading_title'],
                                       'href' => $this->url->link('extension/module/biscuit_prizes')
                                       );
        $data['send'] = false;
        
        // Проверяем тип запроса POST
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            //API Url
            $url = 'http://lock.biscuit.kz/esb/hs/biscuit/reg';
            //Initiate cURL.
            $ch = curl_init($url);
            // The cashed data
            $data['fname'] = $this->request->post['fname'];
            $data['lname'] = $this->request->post['lname'];
            $data['instagram'] = $this->request->post['instagram'];
            $data['email'] = $this->request->post['email'];
            $data['phone'] = $this->request->post['phone'];
            $data['action_code'] = $this->request->post['action_code'];
            $data['agree'] = true;
            //The JSON data.
            $jsonData = array(
                'fname' => $this->request->post['fname'],
                'lname' => $this->request->post['lname'],
                'instagram' => $this->request->post['instagram'],
                'email' => $this->request->post['email'],
                'phone' => $this->request->post['phone'],
                'code' => $this->request->post['action_code']
            );
            //Encode the array into JSON.
            $jsonDataEncoded = json_encode($jsonData);
            //Tell cURL that we want to send a POST request.
            curl_setopt($ch, CURLOPT_POST, true);
            //Tell cURL 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Attach our encoded JSON string to the POST fields.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            //Set the content type to application/json
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
            //Execute the request
            $number = curl_exec($ch);

            $data['number_action'] = '' . $number;
            
            if(!$number) {
                $this->error['warning'] = $this->language->get('error_service_unavailable');
                $data['number_action'] = '';
            }
            
            if(trim($number) == "0") {
                $this->error['warning'] = $this->language->get('error_number_wrong'); 
                $data['number_action'] = '';
                $data['error_action_code'] = 'is-invalid';
                
            }
            
            if($number == "-1") {
                $this->error['warning'] = $this->language->get('error_number_registered'); 
                $data['number_action'] = '';
                $data['error_action_code'] = 'is-invalid';
            }
            
            curl_close ($ch);
            
            $data['send'] = true;
		}
        
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info) {
                $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
            } else {
                $data['text_agree'] = '';
            }
        } else {
            $data['text_agree'] = '';
        }

        // Установить Action для запроса
        $data['action'] = $this->url->link('extension/module/biscuit_prizes', '', true);
        // Set Action check
        $data['action_ch'] = $this->url->link('extension/module/biscuit_prizes/check', '', true); 
        // Set Instagram URL
        $data['accept_registration_d'] = sprintf($this->language->get('accept_registration_d'), 'http://instagram.com/biscuit.qaz', '', '');  
                
        // Загружаем остальное
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        // VAriable for test
        //$data['send'] = true;
        //$data['number_action'] = '13335';
        
        // Выводим на экран
        $this->response->setOutput($this->load->view('extension/module/biscuit_prizes', $data));
    }
    
    public function check() {
        // Загружаем "модель"
        $this->load->model('extension/module/biscuit_prizes');
        $data = array();
        // Загружаем настройки (для проверки включен модуль или нет)
        $data['module_biscuit_prizes_status'] = $this->model_extension_module_biscuit_prizes->LoadSettings();
        // Загружаем языковой файл
        $data += $this->load->language('extension/module/biscuit_prizes');
        // Хлебные крошки
        $data['breadcrumbs'][] = array(
                                       'text' => $this->language->get('text_home'),
                                       'href' => $this->url->link('common/home')
                                       );
        $data['breadcrumbs'][] = array(
                                       'text' => $data['heading_title'],
                                       'href' => $this->url->link('extension/module/biscuit_prizes')
                                       );
        $data['send'] = false;
        $data['send_ch'] = false;
        
        // Проверяем тип запроса POST
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatech()) {
            //API Url
            $url = 'http://lock.biscuit.kz/esb/hs/biscuit/check';
            //Initiate cURL.
            $ch = curl_init($url);
            // The cashed data
            $data['emailcheck'] = $this->request->post['emailcheck'];
            //The JSON data.
            $jsonData = array(
                'email' => $this->request->post['emailcheck']
            );
            //Encode the array into JSON.
            $jsonDataEncoded = json_encode($jsonData);
            //Tell cURL that we want to send a POST request.
            curl_setopt($ch, CURLOPT_POST, true);
            //Tell cURL 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Attach our encoded JSON string to the POST fields.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            //Set the content type to application/json
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
            //Execute the request
            $number = curl_exec($ch);

            $data['number_action'] = '' . $number;
            
            if(!$number) {
                $this->error['warning_ch'] = $this->language->get('error_service_unavailable');
                $data['number_action'] = '';
            }
            
            if(trim($number) == "0") {
                $this->error['warning_ch'] = $this->language->get('error_number_check'); 
                $data['number_action'] = '';
                $data['error_action_code'] = 'is-invalid';
                
            }
            
            curl_close ($ch);
            
            $data['send_ch'] = true;
		}
        
        if (isset($this->error['warning_ch'])) {
            $data['error_warning_ch'] = $this->error['warning_ch'];
        } else {
            $data['error_warning_ch'] = '';
        }
        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info) {
                $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
            } else {
                $data['text_agree'] = '';
            }
        } else {
            $data['text_agree'] = '';
        }

        // Установить Action для запроса
        $data['action'] = $this->url->link('extension/module/biscuit_prizes', '', true);
        // Set Action check
        $data['action_ch'] = $this->url->link('extension/module/biscuit_prizes/check', '', true); 
        // Set Instagram URL
        $data['accept_registration_d'] = sprintf($this->language->get('accept_registration_d'), 'http://instagram.com/biscuit.qaz', '', '');  
                
        // Загружаем остальное
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        // VAriable for test
        //$data['send'] = true;
        //$data['number_action'] = '13335';
        
        // Выводим на экран
        $this->response->setOutput($this->load->view('extension/module/biscuit_prizes', $data));
    }
    
    private function validate() {
		if ((utf8_strlen(trim($this->request->post['fname'])) < 1) || (utf8_strlen(trim($this->request->post['fname'])) > 32)) {
			$this->error['fname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lname'])) < 1) || (utf8_strlen(trim($this->request->post['lname'])) > 32)) {
			$this->error['lname'] = $this->language->get('error_lastname');
		}

        if ((utf8_strlen(trim($this->request->post['instagram'])) < 1) || (utf8_strlen(trim($this->request->post['instagram'])) > 32)) {
			$this->error['instagram'] = $this->language->get('error_instagram');
		}
        
		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['phone']) < 3) || (utf8_strlen($this->request->post['phone']) > 32)) {
			$this->error['phone'] = $this->language->get('error_phone');
		}

        if ((utf8_strlen($this->request->post['action_code']) < 3) || (utf8_strlen($this->request->post['action_code']) > 32)) {
			$this->error['action_code'] = $this->language->get('error_code');
		}
		
		return !$this->error;
	}
    
    private function validatech() {
        
		if ((utf8_strlen($this->request->post['emailcheck']) > 96) || !filter_var($this->request->post['emailcheck'], FILTER_VALIDATE_EMAIL)) {
			$this->error['emailcheck'] = $this->language->get('error_email');
		}
		
		return !$this->error;
	}
}
