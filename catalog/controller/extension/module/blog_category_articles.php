<?php
class ControllerExtensionModuleBlogCategoryArticles extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/blog_category_articles');

		$this->load->model('blog/article');
		$this->load->model('blog/category');

		$this->load->model('tool/image');

		$data['articles'] = array();

		if (!$setting['limit'])
			$setting['limit'] = 3;

		$data['articles'] = array();

		$article_data = array(
			'filter_blog_category_id' => $setting['category_id'],
			'order'                   => 'DESC',
			'limit'                   => $setting['limit'],
			'start'                   => 0
		);

		$results = $this->model_blog_article->getArticles($article_data);
		$data['category'] = $this->model_blog_category->getCategory($setting['category_id']);

		foreach ($results as $result) {
			if ($result['image']) {
				$image = "/image/{$result['image']}";
//				$image = $this->model_tool_image->resize($result['image'], $this->config->get('configblog_image_article_width'), $this->config->get('configblog_image_article_height'));
			} else {
				$image = "/image/placeholder.png";
//				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('configblog_image_article_width'), $this->config->get('configblog_image_article_height'));
			}

			$data['articles'][] = array(
				'article_id'  => $result['article_id'],
				'thumb'       => $image,
				'name'        => $result['name'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('configblog_article_description_length')) . '..',
				'href'        => $this->url->link('blog/article', 'blog_category_id=' . $setting['category_id'] . '&article_id=' . $result['article_id'])
			);
		}

		if ($data['articles'])
			return $this->load->view('extension/module/blog_category_articles', $data);
	}
}