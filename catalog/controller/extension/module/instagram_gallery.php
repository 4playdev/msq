<?php
class ControllerExtensionModuleInstagramGallery extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/instagram_gallery');

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		if (isset($setting['title'][$this->config->get('config_language_id')]))
			$data['title'] = $setting['title'][$this->config->get('config_language_id')];
		else
			$data['title'] = null;

		if (isset($setting['link']))
			$data['link'] = $setting['link'];
		else
			$data['link'] = null;

		$data['banners'] = array();
		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => str_replace("/n","<br />",$result['title']),
                    'link'  => $result['link'],
					'image' => "/image/{$result['image']}"
				);
			}
		}

		return $this->load->view('extension/module/instagram_gallery', $data);
	}
}