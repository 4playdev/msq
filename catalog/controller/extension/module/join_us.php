<?php
class ControllerExtensionModuleJoinUs extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/join_us');

		if (isset($setting['title'][$this->config->get('config_language_id')]))
			$data['title'] = $setting['title'][$this->config->get('config_language_id')];
		else
			$data['title'] = null;

		if (isset($setting['description'][$this->config->get('config_language_id')]))
			$data['description'] = $setting['description'][$this->config->get('config_language_id')];
		else
			$data['description'] = null;

		$data['action'] = '/index.php';

		return $this->load->view('extension/module/join_us', $data);
	}
}