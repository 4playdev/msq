<?php
class ControllerExtensionModuleMsqCards extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/msq_cards');

		$this->load->model('design/banner');
		$this->load->model('tool/image');

        $idx = (int)$this->config->get('config_language_id');
            
		if (isset($setting['title'][$this->config->get('config_language_id')]))
			$data['title'] = $setting['title'][$idx];
		else
			$data['title'] = null;

		if (isset($setting['link']))
			$data['link'] = $setting['link'];
		else
			$data['link'] = null;
        
        if (isset($setting['type']))
			$data['type'] = $setting['type'];
		else
			$data['type'] = null;
        
        if (isset($setting['b_color']))
			$data['b_color'] = $setting['b_color'];
		else
			$data['b_color'] = null;

        
        
            
		$data['banners'] = array();
		$results = $this->model_design_banner->getBanner($setting['banner_id_1']);
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => str_replace("/n","<br />",$setting['b_title_1'][$idx]),
                    'link'  => $result['link'],
                    'type'  => $setting['b_type_1'],
					'image' => "/image/{$result['image']}"
				);
			}
		}
        $results = $this->model_design_banner->getBanner($setting['banner_id_2']);
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => str_replace("/n","<br />",$setting['b_title_2'][$idx]),
                    'link'  => $result['link'],
                    'type'  => $setting['b_type_2'],
					'image' => "/image/{$result['image']}"
				);
			}
		}
        $results = $this->model_design_banner->getBanner($setting['banner_id_3']);
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => str_replace("/n","<br />",$setting['b_title_3'][$idx]),
                    'link'  => $result['link'],
                    'type'  => $setting['b_type_3'],
					'image' => "/image/{$result['image']}"
				);
			}
		}
		return $this->load->view('extension/module/msq_cards', $data);
	}
}