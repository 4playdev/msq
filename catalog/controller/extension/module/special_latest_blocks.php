<?php
class ControllerExtensionModuleSpecialLatestBlocks extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/special_latest_blocks');

		$this->load->model('catalog/product');
		$results['special'] = $this->model_catalog_product->getProduct($setting['special_product_id']);
		$results['latest'] = $this->model_catalog_product->getProduct($setting['latest_product_id']);

		foreach ($results as $index => $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
			}

			$data['products'][$index] = array(
				'product_id'  => $result['product_id'],
				'thumb'       => $image,
				'name'        => $result['name'],
				'image'       => '/image/' . $result['image'],
				'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
			);
		}

		return $this->load->view('extension/module/special_latest_blocks', $data);
	}
}