<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerInformationMap extends Controller {
	public function index() {
		$this->load->language('information/map');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		

		date_default_timezone_set("UTC");
		$time = time();
		$time += 6 * 3600;
		$data['now'] = [
			'h' => date("H", $time),
			'm' => date("i", $time),
		];

		$this->response->setOutput($this->load->view('information/map', $data));
	}
}