<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_items']     = 'Items: %s (%s)';
$_['text_empty']     = 'Your cart is empty!';
$_['text_cart']      = 'Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Billing profile';