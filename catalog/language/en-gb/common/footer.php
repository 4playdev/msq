<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_information']             = 'Information';
$_['text_service']                 = 'Service';
$_['text_extra']                   = 'Extra';
$_['text_contact']                 = 'Contact us';
$_['text_address'] 	               = 'Address';
$_['text_questions'] 	           = 'Questions';
$_['text_return']                  = 'Return item';
$_['text_sitemap']                 = 'Sitemap';
$_['text_manufacturer']            = 'Manufacturer';
$_['text_voucher']                 = 'Voucher';
$_['text_affiliate']               = 'Affiliate';
$_['text_special']                 = 'Special';
$_['text_latest']                  = 'New';
$_['text_bestseller']              = 'Bestseller';
$_['text_mostviewed']              = 'Mostviewed';
$_['text_account']                 = 'Account';
$_['text_my_account']              = 'My account';
$_['text_order']                   = 'Order';
$_['text_wishlist']                = 'Wishlist';
$_['text_newsletter']              = 'Newsletter';
$_['text_about_us']                = 'About us';
$_['text_delivery']                = 'Delivery';
$_['text_addresses']               = 'Our addresses';
$_['text_question_order']          = 'How to order?';
$_['text_question_payment_online'] = 'How to pay online?';
$_['text_question_own']            = 'Own question';
$_['text_powered']                 = 'Powered by <a target="_blank" href="https://ocstore.com/">ocStore</a><br /> %s &copy; %s';
$_['text_send']                    = 'Send';
$_['text_email_notice']            = 'An answer will be sent to You by this e-mail';
$_['text_success']                 = 'Your question has been sent successfully. The answer will come to the indicated e-mail';
$_['text_setting']                 = 'Setting';
$_['text_reward']                  = 'Reward';
$_['text_lotery'] 				   = 'Registration in draw';
$_['text_instagram']			   = 'Instagram';
$_['text_office_address']		   = 'Republic of Kazakhstan, Almaty city, 050000, Makataev st., 28/1';

// Enter
$_['enter_name']     = 'Your name';
$_['enter_email']    = 'Your E-Mail';
$_['enter_question'] = 'Your question';

// Error
$_['error_email']    = 'Please enter a valid E-Mail';
$_['error_question'] = 'The question must contain at least 10 characters';
$_['error_send']     = 'There was an sending error. Please, try later';
