<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wishlist';
$_['text_shopping_cart'] = 'Cart';
$_['text_category']      = 'Category';
$_['text_account']       = 'Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order';
$_['text_transaction']   = 'Transaction';
$_['text_download']      = 'Download';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_instagram']     = 'Instagram';
$_['text_twitter']       = 'Twitter';
$_['text_vkontakte']     = 'VKontakte';
$_['text_home']          = 'Home';
$_['text_latest']        = 'New';
$_['text_catalog']       = 'Catalog';
$_['text_about_us']      = 'About us';
$_['text_help']          = 'Help';
$_['text_contact']       = 'Contact';
$_['text_lotery']		 = 'Registration in draw';