<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_language'] = 'Language';
$_['text_en-gb']    = 'English';
$_['text_en-en']    = 'English';
$_['text_ru-ru']    = 'Russian';
$_['text_kk-kz']    = 'Kazakh';