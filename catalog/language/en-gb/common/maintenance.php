<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Service mode';

// Text
$_['text_maintenance'] = 'Service mode';
$_['text_message']      = '<h1 style="text-align:center;">The store is temporarily closed: we carry out maintenance work.<br/>The store will be available soon. Please check back later.</h1>';