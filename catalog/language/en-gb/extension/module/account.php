<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Account';

// Text
$_['text_register']    = 'Register';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Forgot the password?';
$_['text_account']     = 'Account';
$_['text_edit']        = 'Edit';
$_['text_password']    = 'Password';
$_['text_address']     = 'Address';
$_['text_wishlist']    = 'Wishlist';
$_['text_order']       = 'Order';
$_['text_reward']      = 'Reward';
$_['text_download']    = 'Download';
$_['text_return']      = 'Return';
$_['text_transaction'] = 'Transaction';
$_['text_newsletter']  = 'E-Mail newsletter';
$_['text_recurring']   = 'Recurring';