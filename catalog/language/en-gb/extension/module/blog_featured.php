<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading 
$_['heading_title'] = 'Featured';
$_['text_views'] 	= 'Views:';
$_['button_more']   = 'more';

// Text
$_['text_reviews']  = 'On the basis of %s reviews.'; 