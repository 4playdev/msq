<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Map';
$_['heading_map_menu'] = 'Addresses';

// Text
$_['text_near']   = 'Shops nearby';
$_['text_search']     = 'Search';
$_['text_address']     = 'Addresses';