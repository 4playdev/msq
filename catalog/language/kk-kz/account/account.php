<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']      = 'Жеке кабинет';

// Text
$_['text_account']       = 'Жеке кабинет';
$_['text_my_account']    = 'Менің тіркеу жазбам';
$_['text_my_orders']     = 'Менің тапсырыстарым';
$_['text_my_affiliate']  = 'Серіктестік бөлім';
$_['text_my_newsletter'] = 'Жазылу';
$_['text_edit']          = 'Байланыс ақпаратын өзгерту';
$_['text_password']      = 'Құпия сөзімді өзгерту';
$_['text_address']       = 'Мекенжайларымды өзгерту';
$_['text_credit_card']   = 'Сақталған несие карталарын басқару';
$_['text_account_wishlist']      = 'Таңдаулыны өзгерту';
$_['text_order']         = 'Тапсырыстар тарихы';
$_['text_download']      = 'Жүктеу файлдары';
$_['text_reward']        = 'Бонусты ұпайлар';
$_['text_return']        = 'Қайтару сұраулары';
$_['text_transaction']   = 'Фин. операцияларынаң тарихы';
$_['text_newsletter']    = 'Жаңалықтарды таратуға жазылу немесе жазудан бас тарту';
$_['text_recurring']     = 'Тұрақты төлемдер';
$_['text_transactions']  = 'Операциялар';
$_['text_affiliate_add']  = 'Серіктестік аккаунтын тіркеу';
$_['text_affiliate_edit'] = 'Серіктестік ақпаратын өзгерту';
$_['text_tracking']       = 'Серіктестерді бақылау коды';