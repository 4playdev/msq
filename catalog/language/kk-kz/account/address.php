<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']        = 'Менің мекенжайларым';

// Text
$_['text_account']         = 'Жеке кабинет';
$_['text_address_book']    = 'Жеткізу мекенжайларының тізімі';
$_['text_edit_address']    = 'Мекенжайды өңдеу';
$_['text_add']             = 'Сіздің мекенжайыңыз сәтті қосылды';
$_['text_edit']            = 'Сіздің мекенжайыңыз сәтті өзгертілді';
$_['text_delete']          = 'Сіздің мекенжайыңыз сәтті жойылды';
$_['text_empty']           = 'Сіздің тіркелгіңізде мекенжай жоқ';

// Entry
$_['entry_firstname']      = 'Аты';
$_['entry_lastname']       = 'Жөні';
$_['entry_company']        = 'Компания';
$_['entry_address_1']      = 'Мекенжай 1';
$_['entry_address_2']      = 'Мекенжай 2';
$_['entry_postcode']       = 'Пошталық индекс';
$_['entry_city']           = 'Қала';
$_['entry_country']        = 'Ел';
$_['entry_zone']           = 'Аймақ / Облыс';
$_['entry_default']        = 'Негізгі мекенжай';

// Error
$_['error_delete']         = 'Сізде 1 мекен-жайдан кем болмауы керек!';
$_['error_default']        = 'Негізгі адресті жою мүмкін емес!';
$_['error_firstname']      = 'Аты 1-ден 32 таңбаға дейін болуы керек!';
$_['error_lastname']       = 'Жөні 1-ден 32 таңбаға дейін болуы керек!';
$_['error_vat']            = 'Қате VAT number!';
$_['error_address_1']      = 'Мекен-жай 3-тен 128 символға дейін болуы тиіс!';
$_['error_postcode']       = 'Индекс 2-ден 10 символға дейін болуы тиіс!';
$_['error_city']           = 'Қаланың атауы 2-ден 128 символға дейін болуы тиіс!';
$_['error_country']        = 'Елді көрсетіңіз!';
$_['error_zone']           = 'Аймақ/Облысты көрсетіңіз!';
$_['error_custom_field']   = '%s қажетті!';