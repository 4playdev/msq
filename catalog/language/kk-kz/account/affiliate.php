<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']             = 'Сіздің серіктестік ақпаратыңыз';

// Text
$_['text_account']              = 'Жеке кабинет';
$_['text_affiliate']            = 'Серіктестік бөлім';
$_['text_my_affiliate']         = 'Менің серіктестік аккаунтым';
$_['text_payment']              = 'Төлем ақпараты';
$_['text_cheque']               = 'Чек';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Банктік аударым';
$_['text_success']              = 'Тіркелгіңіз сәтті жаңартылды.';
$_['text_agree']                = '<a href="%s" class="agree"><b>%s</b></a> оқып келісемін.';

// Entry
$_['entry_company']             = 'Компания';
$_['entry_website']             = 'Веб-сайт';
$_['entry_tax']                 = 'Салық коды (ЖСН)';
$_['entry_payment']             = 'Төлем тәсілі';
$_['entry_cheque']              = 'Чек, Төлем алушының аты';
$_['entry_paypal']              = 'PayPal Email аккаунт';
$_['entry_bank_name']           = 'Банктің атауы';
$_['entry_bank_branch_number']  = 'ABA/BSB нөмір (бөлімше нөмірі)';
$_['entry_bank_swift_code']     = 'SWIFT коды';
$_['entry_bank_account_name']   = 'Шот атауы';
$_['entry_bank_account_number'] = 'Шот нөмірі';

// Error
$_['error_agree']               = 'Сіз келісуді %s растауыңыз керек!';
$_['error_cheque']              = 'Алушының атын тексеріңіз!';
$_['error_paypal']              = 'PayPal электрондық пошта мекенжайы жарамды емес!';
$_['error_bank_account_name']   = 'Шот атауы талап етіледі!';
$_['error_bank_account_number'] = 'Шот нөмірі талап етіледі!';
$_['error_custom_field']        = '%s талап етіледі!';