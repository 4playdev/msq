<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']     = 'Жүктеу файлдары';

// Text
$_['text_account']      = 'Жеке кабинет';
$_['text_downloads']    = 'Жүктеу файлдары';
$_['text_empty']        = 'Жүктеу үшін қол жетімді файлдар жоқ!';

// Column
$_['column_order_id']   = '№ Тапсырыс';
$_['column_name']       = 'Атауы';
$_['column_size']       = 'Өлшемі';
$_['column_date_added'] = 'Қосу күні';