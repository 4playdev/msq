<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']      = 'Тіркелгі';

// Text
$_['text_account']       = 'Жеке кабинет';
$_['text_edit']          = 'Ақпаратты өңдеу';
$_['text_your_details']  = 'Сіздің тіркелгіңіз';
$_['text_success']       = 'Сіздің тіркелгіңіз сәтті жаңартылды!';

// Entry
$_['entry_firstname']    = 'Аты';
$_['entry_lastname']     = 'Жөні';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Телефон';
$_['entry_fax']          = 'Факс';

// Error
$_['error_exists']       = 'Мұндай E-Mail тіркелген!';
$_['error_firstname']    = 'Аты 1-ден 32 таңбаға дейін болуы керек!';
$_['error_lastname']     = 'Тегі 1-ден 32 символға дейін болуы тиіс!';
$_['error_email']        = 'E-Mail мекенжайы дұрыс емес!';
$_['error_telephone']    = 'Телефон нөмірі 3-тен 32 символға дейін болуы керек!';
$_['error_custom_field'] = '%s талап етіледі!';