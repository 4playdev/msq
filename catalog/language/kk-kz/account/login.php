<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']                = 'Авторизация';

// Text
$_['text_account']                 = 'Аккаунт';
$_['text_login']                   = 'Авторизация';
$_['text_new_customer']            = 'Жаңа сатып алушы';
$_['text_register']                = 'Тіркелу';
$_['text_register_account']        = 'Тіркелгіні жасау сатып алуды жылдам әрі ыңғайлы етуге көмектеседі. Сіз сондай-ақ тапсырысыңыздың мәртебесін қадағалап, бетбелгілерді пайдалана аласыз, алдыңғы тапсырыстарыңызды көре аласыз немесе біздің тұрақты сатып алушы ретінде жеңілдік аласыз.';
$_['text_returning_customer']      = 'Тұрақты сатып алушы';
$_['text_i_am_returning_customer'] = 'Мен бұл жерде бұрын сатып алдым және тіркелдім';
$_['text_forgotten']               = 'Құпия сөзіңізді ұмыттыңыз ба?';

// Entry
$_['entry_email']                  = 'E-Mail мекенжай';
$_['entry_password']               = 'Құпия сөз';

// Error
$_['error_login']                  = 'Енгізілген e-Mail мекенжайы және/немесе құпия сөз табылмады.';
$_['error_attempts']               = 'Тіркелгіңіз жүйеге кіру әрекеттерінің рұқсат етілген санынан асып кетті. 1 сағаттан кейін тағы көріңіз.';
$_['error_approved']               = 'Сіз дүкен әкімшілігі тіркелгіні тексергеннен кейін кіре аласыз.';