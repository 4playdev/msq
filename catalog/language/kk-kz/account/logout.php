<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Шығу';

// Text
$_['text_message']  = '<p>Вы вышли из Вашего Личного Кабинета. Сіз өзіңіздің Жеке Кабинетіңізден шыңып кеттіңіз.</p><p>Сіздің сауда себетіңіз сақталды. Ол сіздің жеке кабинетіңізге келесі кіру кезінде қалпына келтіріледі.</p>';
$_['text_account']  = 'Жеке кабинет';
$_['text_logout']   = 'Шығу';