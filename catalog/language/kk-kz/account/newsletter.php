<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Жаңалықтарға жазылу';

// Text
$_['text_account']     = 'Жеке кабинет';
$_['text_newsletter']  = 'Тарату';
$_['text_success']     = 'Сіздің жазылымыңыз сәтті жаңартылды!';

// Entry
$_['entry_newsletter'] = 'Жазылу';
