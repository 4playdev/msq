<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']         = 'Тапсырыстар тарихы';

// Text
$_['text_account']          = 'Жеке кабинет';
$_['text_order']            = 'Тапсырыс';
$_['text_order_detail']     = 'Тапсырыс нақтылауы';
$_['text_invoice_no']       = '№ Шот:';
$_['text_account_order_id'] = '№ тапсырыс:';
$_['text_date_added']       = 'Қосу күні:';
$_['text_shipping_address'] = 'Жеткізу мекенжайы';
$_['text_shipping_method']  = 'Жеткізу тәсілі:';
$_['text_payment_address']  = 'Төлем мекенжайы';
$_['text_payment_method']   = 'Төлем тәсілі:';
$_['text_comment']          = 'Тапсырыс пікірлері';
$_['text_history']          = 'Тапсырыс тарихы';
$_['text_success']          = '<a href="%s">%s</a> тапсырысының Тауарлары <a href="%s">Сіздің Себетке</a> сәтті қосылды! ';
$_['text_empty']            = 'Сіз әлі тапсырыс берген жоқсыз!';
$_['text_error']            = 'Сұралған Тапсырыс табылмады!';

// Column
$_['column_order_id']       = '№ Тапсырыс';
$_['column_product']        = 'Тауарлар саны';
$_['column_customer']       = 'Сатып алушы';
$_['column_name']           = 'Тауар атауы';
$_['column_model']          = 'Тауар коды';
$_['column_quantity']       = 'Саны';
$_['column_price']          = 'Бағасы';
$_['column_total']          = 'Барлығы';
$_['column_action']         = 'Әрекет';
$_['column_date_added']     = 'Қосу күні';
$_['column_status']         = 'Күй';
$_['column_comment']        = 'Пікір';

// Error
$_['error_reorder']         = '%s қазіргі уақытта тапсырыс үшін қол жетімді емес.';