<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']  = 'Құпия сөзді ауыстыру';

// Text
$_['text_account']   = 'Жеке кабинет';
$_['text_password']  = 'Сіздің құпия сөзіңіз';
$_['text_success']   = 'Сіздің құпия сөзіңіз сәтті өзгерді!';

// Entry
$_['entry_password'] = 'Құпия сөз';
$_['entry_confirm']  = 'Құпия сөзді растаңыз';

// Error
$_['error_password'] = 'Парольде 4-тен 20 таңбаға дейін болуы тиіс!';
$_['error_confirm']  = 'Құпия сөздер сәйкес келмейді!';