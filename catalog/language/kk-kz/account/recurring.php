<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

$_['heading_title'] 							= 'Тұрақты төлемдер';
$_['button_continue'] 							= 'Жалғастыру';
$_['button_view'] 								= 'Көрсету';
$_['text_empty'] 								= 'Төлем профилі табылмады';
$_['text_product'] 								= 'Тауар:';
$_['text_order'] 								= 'Тапсырыс:';
$_['text_quantity'] 							= 'Саны: ';
$_['text_account'] 								= 'Аккаунт';
$_['text_action'] 								= 'Әрекет';
$_['text_recurring'] 							= 'Тұрақты төлемдер';
$_['text_transactions'] 						= 'Операциялар';
$_['button_return'] 							= 'Қайтару';
$_['text_empty_transactions'] 					= 'Бұл профиль үшін аударма жоқ';

$_['column_date_added'] 						= 'Қосу күні';
$_['column_type'] 								= 'Түрі';
$_['column_amount'] 							= 'Барлығы';
$_['column_status'] 							= 'Күй';
$_['column_product'] 							= 'Тауар';
$_['column_action'] 							= 'Әрекет';
$_['column_recurring_id'] 						= 'Профиль ID-і';

$_['text_recurring_detail'] 					= 'Тұрақты төлем нақтылауы';
$_['text_recurring_id'] 						= 'Профиль ID-і: ';
$_['text_payment_method'] 						= 'Төлем тәсілі: ';
$_['text_date_added'] 							= 'Қосу уақыты: ';
$_['text_recurring_description'] 				= 'Сипаттама: ';
$_['text_status'] 								= 'Күй: ';
$_['text_ref'] 									= 'Ескерту: ';

$_['text_status_active'] 						= 'Қосылулы';
$_['text_status_inactive'] 						= 'Өшірулі';
$_['text_status_cancelled'] 					= 'Болдырылмады';
$_['text_status_suspended'] 					= 'Мұздатылған';
$_['text_status_expired'] 						= 'Мерзімі өтіп кетті';
$_['text_status_pending'] 						= 'Күтуде';

$_['text_transaction_date_added'] 				= 'Қосу уақыты';
$_['text_transaction_payment'] 					= 'Төлем';
$_['text_transaction_outstanding_payment'] 		= 'Түспеген төлем';
$_['text_transaction_skipped'] 					= 'Төлем өтіп кеткен';
$_['text_transaction_failed'] 					= 'Төлем мәселесі';
$_['text_transaction_cancelled'] 				= 'Болдырылмады';
$_['text_transaction_suspended'] 				= 'Мұздатылған';
$_['text_transaction_suspended_failed'] 		= 'Заморожен из-за неудачного платежа Сәтсіз төлемнен кейін мұздатылған';
$_['text_transaction_outstanding_failed'] 		= 'Төлем өтпеді';
$_['text_transaction_expired'] 					= 'Мерзімі өтіп кетті';

$_['error_not_cancelled'] 						= 'Қате: %s';
$_['error_not_found'] 							= 'Профильды өшіруге болмайды';
$_['text_cancelled'] 							= 'Тұрақты төлем өшірілді';