<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']        = 'Тіркелу';

// Text
$_['text_account']         = 'Аккаунт';
$_['text_register']        = 'Тіркелу';
$_['text_account_already'] = 'Егер Сіз тіркелген болсаңыз, <a href="%s" class="link link--black font-weight-bold">Жүйеге Кіру</a> бетіне көшіңіз.';
$_['text_your_details']    = 'Байланыс мәліметтері';
$_['text_your_address']    = 'Сіздің мекенжайыңыз';
$_['text_newsletter']      = 'Жаңалықтар тарату';
$_['text_your_password']   = 'Сіздің құпия сөзіңіз';
$_['text_agree']           = '<a href="%s" class="agree link link--black font-weight-bold"><b>%s</b></a> оқып, шарттарымен келісемін.';

// Entry
$_['entry_customer_group'] = 'Сатып алушылар тобы';
$_['entry_firstname']      = 'Аты';
$_['entry_lastname']       = 'Жөні';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Телефон';
$_['entry_fax']            = 'Факс';
$_['entry_company']        = 'Компания';
$_['entry_address_1']      = 'Мекенжай 1';
$_['entry_address_2']      = 'Мекенжай 2';
$_['entry_postcode']       = 'Пошталық индекс';
$_['entry_city']           = 'Қала';
$_['entry_country']        = 'Ел';
$_['entry_zone']           = 'Аймақ / Облыс';
$_['entry_newsletter']     = 'Жаңалықтарды E-Mail арқылы алу';
$_['entry_password']       = 'Құпия сөз';
$_['entry_confirm']        = 'Құпия сөзді растаңыз';

// Error
$_['error_exists']         = 'Мұндай E-Mail тіркелген!';
$_['error_firstname']      = 'Аты 1-ден 32 таңбаға дейін болуы керек!';
$_['error_lastname']       = 'Жөні 1-ден 32 символға дейін болуы тиіс!';
$_['error_email']          = 'E-Mail адресі дұрыс емес!';
$_['error_telephone']      = 'Телефон нөмірі 3-тен 32 символға дейін болуы керек!';
$_['error_address_1']      = 'Мекен-жай 3-тен 128 символға дейін болуы тиіс!';
$_['error_city']           = 'Қаланың атауы 2-ден 128 символға дейін болуы тиіс!';
$_['error_postcode']       = 'Индекс 2-ден 10 символға дейін болуы тиіс!';
$_['error_country']        = 'Елді көрсетіңіз!';
$_['error_zone']           = 'Аймақты / облысты көрсетіңіз!';
$_['error_custom_field']   = '%s талап етіледі!';
$_['error_password']       = 'Құпия сөзде 4-тен 20 таңбаға дейін болуы тиіс!';
$_['error_confirm']        = 'Құпия сөздер сәйкес келмейді!';
$_['error_agree']          = '%s танысып, келісуіңіз керек!';