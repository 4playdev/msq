<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']  = 'Құпия сөзді қалпына келтіру';

// Text
$_['text_account']   = 'Жеке кабинет';
$_['text_password']  = 'Жаңа құпия сөзіңізді көрсетіңіз.';
$_['text_success']   = 'Сіздің құпия сөзіңіз сәтті өзгертілді.';

// Entry
$_['entry_password'] = 'Құпия сөз';
$_['entry_confirm']  = 'Құпия сөзді растаңыз';

// Error
$_['error_password'] = 'Құпия сөзде 4-тен 20 таңбаға дейін болуы тиіс!';
$_['error_confirm']  = 'Құпия сөздер сәйкес келмейді!';
$_['error_code']     = 'Құпия сөзді қалпына келтіру коды жарамсыз немесе бұрын пайдаланылған!';