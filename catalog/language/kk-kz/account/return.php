<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']      = 'Тауарды қайтару';

// Text
$_['text_account']       = 'Аккаунт';
$_['text_return']        = 'Қайтару туралы ақпарат';
$_['text_return_detail'] = 'Қайтару туралы толық ақпарат';
$_['text_description']   = 'Тауарды қайтаруға сұрау нысанын толтырыңыз.';
$_['text_order']         = 'Тапсырыс туралы ақпарат';
$_['text_product']       = 'Тауар туралы ақпарат және қайтару себебі';
$_['text_message']       = '<p>Сіз қайтару сұрауын жібердіңіз.</p><p> Сұраудың мәртебесі туралы хабарлама Сіздің e-mail-ге келеді.</p>';
$_['text_return_id']     = 'Қайтару үшін сұрау салу №:';
$_['text_order_id']      = 'Тапсырыс №:';
$_['text_date_ordered']  = 'Тапсырыс күні:';
$_['text_status']        = 'Күй:';
$_['text_date_added']    = 'Қосу күні:';
$_['text_comment']       = 'Қайтару бойынша түсініктеме';
$_['text_history']       = 'Қайтару тарихы';
$_['text_empty']         = 'Сізде бұрын тауарларды қайтару болған жоқ!';
$_['text_agree']         = 'Мен <a href="%s" class="agree"><b>%s</b></a> оқып, шарттарымен келісемін.';

// Column
$_['column_return_id']   = 'Қайтару үшін сұрау салу №:';
$_['column_order_id']    = 'Тапсырыс №:';
$_['column_status']      = 'Күй';
$_['column_date_added']  = 'Қосу күні';
$_['column_customer']    = 'Сатып алушы';
$_['column_product']     = 'Тауар атауы';
$_['column_model']       = 'Тауар коды';
$_['column_quantity']    = 'Саны';
$_['column_price']       = 'Бағасы';
$_['column_opened']      = 'Ашық';
$_['column_comment']     = 'Пікір';
$_['column_reason']      = 'Себеп';
$_['column_action']      = 'Әрекет';

// Entry
$_['entry_order_id']     = 'Тапсырыс №';
$_['entry_date_ordered'] = 'Тапсырыс күні';
$_['entry_firstname']    = 'Аты';
$_['entry_lastname']     = 'Жөні';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Телефон';
$_['entry_product']      = 'Тауар атауы';
$_['entry_model']        = 'Тауар коды';
$_['entry_quantity']     = 'Саны';
$_['entry_reason']       = 'Қайтару себебі';
$_['entry_opened']       = 'Туар шешілді';
$_['entry_fault_detail'] = 'Ақаулардың сипаттамасы';

// Error
$_['text_error']         = 'Қайтару сұрау табылмады!';
$_['error_order_id']     = 'Тапсырыс № көрсетілмеген!';
$_['error_firstname']    = 'Аты 1-ден 32 таңбаға дейін болуы керек!';
$_['error_lastname']     = 'Жөні 1-ден 32 символға дейін болуы тиіс!';
$_['error_email']        = 'E-Mail адресі дұрыс емес!';
$_['error_telephone']    = 'Телефон нөмірі 3-тен 32 символға дейін болуы керек!';
$_['error_product']      = 'Тауардың атауы 3-тен 255 символға дейін болуы тиіс!';
$_['error_model']        = 'Тауар коды 3-тен 64 символға дейін болуы тиіс!';
$_['error_reason']       = 'Тауарды қайтару себебін көрсету қажет!';
$_['error_agree']        = '%s келісімін растау керек!';