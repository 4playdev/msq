<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']      = 'Бонустық ұпайлар';

// Column
$_['column_date_added']  = 'Қосу күні';
$_['column_description'] = 'Сипаттамасы';
$_['column_points']      = 'Бонустық ұпайлар';

// Text
$_['text_account']       = 'Жеке кабинет';
$_['text_reward']        = 'Бонустық ұпайлар';
$_['text_total']         = 'Бонустық ұпайлар жинақталған саны:';
$_['text_empty']         = 'Сізде бонустық ұпай жоқ!';