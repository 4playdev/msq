<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Реферальды сілтемелер';

// Text
$_['text_account']     = 'Серіктес Кабинеті';
$_['text_description'] = 'Сатып алушылардың сатып алуларынан агенттік сыйақы алу үшін сілтеме кодын қосу қажет. %s сайты үшін сілтеме генераторын пайдаланыңыз.';

// Entry
$_['entry_code']       = 'Сіздің Реферальды Кодыңыз';
$_['entry_generator']  = 'Реферальды Сізтеме Генераторы';
$_['entry_link']       = 'Реферальды сілтемелер';

// Help
$_['help_generator']  = 'Сілтемені жасау керек тауардың атауын енгізіңіз';