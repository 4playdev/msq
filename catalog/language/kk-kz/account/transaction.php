<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']      = 'Қаржы операцияларының тарихы';

// Column
$_['column_date_added']  = 'Қосу күні';
$_['column_description'] = 'Сипаттамасы';
$_['column_amount']      = 'Сомасы (%s)';

// Text
$_['text_account']       = 'Жеке кабинет';
$_['text_transaction']   = 'Қаржы операциялары';
$_['text_total']         = 'Сіздің ағымдағы балансыңыз:';
$_['text_empty']         = 'Қаржылық операциялар болған жоқ!';