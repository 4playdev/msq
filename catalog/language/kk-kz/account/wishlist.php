<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Таңдаулы';

// Text
$_['text_account']  = 'Жеке кабинет';
$_['text_instock']  = 'Қолда бар';
$_['text_wishlist'] = 'Таңдаулы';
$_['text_login']    = '<a href="%s">%s</a> тауарын <a href="%s">Таңдаулыға</a> сақтау үшін, <a href="%s"">Жеке кабинетке</a> кіру кажет немесе <a href="%s">Тіркелгіні жасау</a> керек!';
$_['text_success']  = '<a href="%s">%s</a> тауары <a href="%s">Таңдаулыға</a> қосылды!';
$_['text_remove']   = 'Избранные успешно обновлены! Таңдаулылар сәтті жаңартылды!';
$_['text_empty']    = 'Сіздің Таңдаулыларыңыз бос.';

// Column
$_['column_image']  = 'Сурет';
$_['column_name']   = 'Тауар атауы';
$_['column_model']  = 'Тауар коды';
$_['column_stock']  = 'Қолда бар';
$_['column_price']  = 'Тауар бірлігінің бағасы';
$_['column_action'] = 'Әрекет';