<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']        = 'Серіктестік бөлім';

// Text
$_['text_account']         = 'Серіктес Кабинеті';
$_['text_my_account']      = 'Менің тіркелгім';
$_['text_my_tracking']     = 'Менің рефералдарым';
$_['text_my_transactions'] = 'Төлемдер тарихы';
$_['text_edit']            = 'Тіркелгіні өңдеу';
$_['text_password']        = 'Құпия сөзді өзгерту';
$_['text_payment']         = 'Төлемдер деректемелерін өзгерту';
$_['text_tracking']        = 'Реферальды коды';
$_['text_transaction']     = 'Төлемдер тарихын қарау';