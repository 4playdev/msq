<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']     = 'Менің ақпаратым';

// Text
$_['text_account']      = 'Аккаунт';
$_['text_edit']         = 'Ақпаратты редакциялау';
$_['text_your_details'] = 'Тіркелгіңіз';
$_['text_your_address'] = 'Мекенжайыңыз';
$_['text_success']      = 'Тіркелгіңіз сәтті жаңартылды!';

// Entry
$_['entry_firstname']   = 'Аты';
$_['entry_lastname']    = 'Жөні';
$_['entry_email']       = 'E-Mail';
$_['entry_telephone']   = 'Телефон';
$_['entry_fax']         = 'Факс';
$_['entry_company']     = 'Компания';
$_['entry_website']     = 'Веб-сайт';
$_['entry_address_1']   = 'Мекенжай 1';
$_['entry_address_2']   = 'Мекенжай 2';
$_['entry_postcode']    = 'Пошталық индекс';
$_['entry_city']        = 'Қала';
$_['entry_country']     = 'Ел';
$_['entry_zone']        = 'Аймақ/Облыс';

// Error
$_['error_exists']      = 'Мұндай E-Mail тіркелген!';
$_['error_firstname']   = 'Аты 1-ден 32 таңбаға дейін болуы керек!';
$_['error_lastname']    = 'Тегі 1-ден 32 символға дейін болуы тиіс!';
$_['error_email']       = 'E-Mail адресі дұрыс емес!';
$_['error_telephone']   = 'Телефон нөмірі 3-тен 32 символға дейін болуы керек!';
$_['error_address_1']   = 'Мекенжайы 3-тен 128 символға дейін болуы тиіс!';
$_['error_city']        = 'Қаланың атауы 2-ден 128 символға дейін болуы тиіс!';
$_['error_country']     = 'Елді көрсетіңіз!';
$_['error_zone']        = 'Аймақ/Облысты көрсетіңіз!';
$_['error_postcode']    = 'Индекс 2-ден 10 символға дейін болуы тиіс!';