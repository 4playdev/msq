<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']   = 'Құпия сөзіңізді ұмыттыңыз ба?';

// Text
$_['text_account']    = 'Серіктес Кабинеті';
$_['text_forgotten']  = 'Құпия сөзіңізді ұмыттыңыз ба?';
$_['text_your_email'] = 'Сіздің E-Mail';
$_['text_email']      = 'Тіркелгіңіздің электрондық пошта мекенжайын енгізіңіз. Электрондық пошта арқылы парольді алу үшін Жалғастыру түймешігін басыңыз.';
$_['text_success']    = 'Жаңа құпия сөз Сіздің электрондық пошта мекенжайыңызға жіберілді.';

// Entry
$_['entry_email']     = 'E-Mail мекенжайы';

// Error
$_['error_email']     = 'E-Mail мекенжайы табылмады, тексеріңіз және тағы да көріңіз!!';
$_['error_approved']  = 'Тіркелгі сіз кіре алмас бұрын мақұлдауды талап етеді.';