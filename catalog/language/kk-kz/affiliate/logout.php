<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Серіктес Кабинетінен шығу';

// Text
$_['text_message']  = '<p>Сіз Серіктес Кабинетінен шықтыңыз.</p>';
$_['text_account']  = 'Серіктес Кабинеті';
$_['text_logout']   = 'Шығу';