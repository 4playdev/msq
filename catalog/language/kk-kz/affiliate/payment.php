<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']             = 'Төлем тәсілі';

// Text
$_['text_account']              = 'Аккаунт';
$_['text_payment']              = 'Төлем';
$_['text_your_payment']         = 'Төлем деректемелері';
$_['text_your_password']        = 'Сіздің құпия сөзіңіз';
$_['text_cheque']               = 'Чек';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Банктік аударым';
$_['text_success']              = 'Тіркелгі сәтті жаңартылды!';

// Entry
$_['entry_tax']                 = 'Салық коды (ИНН)';
$_['entry_payment']             = 'Төлем тәсілі';
$_['entry_cheque']              = 'Чек, Төлем алушының аты';
$_['entry_paypal']              = 'PayPal Email аккаунт';
$_['entry_bank_name']           = 'Банктің атауы';
$_['entry_bank_branch_number']  = 'ABA/BSB нөмір (бөлім нөмірі)';
$_['entry_bank_swift_code']     = 'SWIFT коды';
$_['entry_bank_account_name']   = 'Шот атауы';
$_['entry_bank_account_number'] = 'Шот нөмірі';