<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_blog']         = 'Блог';
$_['text_write']        = 'Пікір жазу';
$_['text_login']        = 'Пікір жазбас бұрын <a href= "%s ">Авторизациядан өтіңіз< / a> немесе <a href= "%s">Тіркелгіні жасаңыз</a>';
$_['text_no_reviews']   = 'Бұл мақалаға әзірше пікір жоқ.';
$_['text_on']           = ' бастап ';
$_['text_note']         = '<span style="color: #FF0000;">Назар аудырыңыз:</span> HTML қолдау көрсетілмейді! Қалыпты мәтінді пайдаланыңыз.';
$_['text_success']      = 'Пікір қалдырғаныңыз үшін рахмет. Әкімші тексергеннен кейін ол жарияланады.';
$_['text_wait']         = 'Күте тұруыңызды өтінеміз!';
$_['text_reviews']      = 'Пікірлер: %s';
$_['text_error']        = 'Мақала табылмады!';
$_['text_views'] 		= 'Көрсетілімдер:';
$_['button_more']       = 'толығырақ';
$_['text_tax']          = 'ҚҚС-сыз:';
$_['text_related']       = 'Ұқсас мақалалар';
$_['text_related_product']       = 'Ілеспе Тауарлар';

// Entry
$_['entry_name']        = 'Сіздің атыңыз:';
$_['entry_review']      = 'Сіздің пікіріңіз:';
$_['entry_rating']      = 'Баға:';
$_['entry_good']        = 'Жақсы';
$_['entry_bad']         = 'Жаман';
$_['entry_captcha']     = 'Суретте көрсетілген кодты енгізіңіз:';

// Error
$_['error_name']        = 'Пікір тақырыбы 3-тен 25 таңбаға дейін болуы керек!';
$_['error_text']        = 'Пікір мәтіні 25-тен 1000 символға дейін болуы керек!';
$_['error_rating']      = 'Мақаланы бағалаңыз!';
$_['error_captcha']     = 'Суретте көрсетілген код қате енгізілген!';
?>