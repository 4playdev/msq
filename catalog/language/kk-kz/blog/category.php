<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_blog']         = 'Блог';
$_['text_refine']       = 'Санатты таңдаңыз';
$_['text_product']      = 'Мақалалар';
$_['text_error']        = 'Санат табылмады!';
$_['text_empty']        = 'Бұл санатта жазбалар жоқ.';
$_['text_display']      = 'Түр:';
$_['text_sort']         = 'Сұрыптау:';
$_['text_limit']        = 'Бетте:';
$_['text_sort_date']    = 'күнге';
$_['text_sort_by']      = 'Бойынша сұрыптау:';
$_['text_sort_name']    = 'атауы';
$_['text_sort_rated']   = 'рейтингі';
$_['text_sort_viewed']  = 'көрсетілім';
$_['text_views'] 	    = 'Көрсетілімдер:';
$_['text_date_asc']     = 'Күні (өсу ретімен)';
$_['text_date_desc']    = 'Күні (кему ретімен)';
$_['text_viewed_asc'] 	= 'Көрсетілімдер (өсу ретімен)';
$_['text_viewed_desc'] 	= 'Көрсетілімдер (кему ретімен)';
$_['text_rating_asc'] 	= 'Рейтингі (өсу ретімен)';
$_['text_rating_desc'] 	= 'Рейтингі (кему ретімен)';
$_['text_name_asc'] 	= 'Аты (өсу ретімен)';
$_['text_name_desc'] 	= 'Аты (кему ретімен)';
$_['text_default']   	= 'Әдетті';

$_['button_more']       = 'толығырақ';
?>