<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']     = 'Блог';

// Text
$_['text_product']      = 'Мақалалар';
$_['text_display']      = 'Түр:';
$_['text_sort']         = 'Сүрыптау:';
$_['text_limit']        = 'Бетте:';
$_['text_sort_date']    = 'күні';
$_['text_list']         = 'тізіммен';
$_['text_grid']         = 'кестемен';
$_['text_sort_by']      = 'Бойынша сұрыптау:';
$_['text_sort_name']    = 'атауы';
$_['text_sort_rated']   = 'рейтингі';
$_['text_sort_viewed']  = 'көрсетілім';
$_['text_views'] 	    = 'Көрсетілімдер:';
$_['text_date_asc']     = 'Күні (өсу ретімен)';
$_['text_date_desc']    = 'Күні (кему ретімен)';
$_['text_viewed_asc'] 	= 'Көрсетілімдер (өсу ретімен)';
$_['text_viewed_desc'] 	= 'Көрсетілімдер (кему ретімен)';
$_['text_rating_asc'] 	= 'Рейтингі (өсу ретімен)';
$_['text_rating_desc'] 	= 'Рейтингі (кему ретімен)';
$_['text_name_asc'] 	= 'Атауы (өсу ретімен)';
$_['text_name_desc'] 	= 'Атауы (кему ретімен)';
$_['text_default']   	= 'Әдетті';
$_['text_empty']        = 'Блогта жазбалар жоқ.';

$_['button_more']       = 'толығырақ';
?>