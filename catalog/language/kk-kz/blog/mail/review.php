<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_subject']	= '%s - мақала тіралы пікір';
$_['text_waiting']	= 'Жаңа пікірлер сіздің тексеруіңізді күтуде.';
$_['text_article']	= 'Мақала: %s';
$_['text_reviewer']	= 'Пікірді қалдырған: %s';
$_['text_rating']	= 'Баға: %s';
$_['text_review']	= 'Пікір:';