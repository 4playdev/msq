<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_items']     = 'Тауарлар: %s (%s)';
$_['text_empty']     = 'Сіздің себетіңіз бос!';
$_['text_cart']      = 'Себет';
$_['text_checkout']  = 'Тапсырысты рәсімдеу';
$_['text_recurring'] = 'Төлем профилі';