<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_information']             = 'Ақпарат';
$_['text_service']                 = 'Қолдау қызметі';
$_['text_extra']                   = 'Қосымша';
$_['text_contact']                 = 'Байланысу';
$_['text_address'] 	               = 'Мекен-жай';
$_['text_questions'] 	           = 'Сұрақтар';
$_['text_return']                  = 'Тауарды қайтару';
$_['text_sitemap']                 = 'Сайт картасы';
$_['text_manufacturer']            = 'Өндірушілер';
$_['text_voucher']                 = 'Сыйлық сертификаттары';
$_['text_affiliate']               = 'Серіктестік бағдарлама';
$_['text_special']                 = 'Акциялар';
$_['text_latest']                  = 'Жаңалықтар';
$_['text_bestseller']              = 'Сатылым хиттары';
$_['text_mostviewed']              = 'Танымал Тауарлар ';
$_['text_account']                 = 'Аккаунт';
$_['text_my_account']              = 'Менің аккаунтым';
$_['text_order']                   = 'Тапсырыс тарихы';
$_['text_wishlist']                = 'Таңдаулы';
$_['text_newsletter']              = 'Тарату';
$_['text_about_us']                = 'Біз туралы';
$_['text_delivery']                = 'Жеткізу';
$_['text_addresses']               = 'Біздің мекенжайларымыз';
$_['text_question_order']          = 'Тапсырысты қалай беруге болады?';
$_['text_question_payment_online'] = 'Онлайн төлеу';
$_['text_question_own']            = 'Өзімнің сұрағым';
$_['text_powered']                 = '<a target="_blank" href="https://ocstore.com/">ocStore</a><br /> %s &copy; %s -да жұмыс істейді';
$_['text_send']                    = 'Жіберу';
$_['text_email_notice']            = 'Жауап осы поштаға жіберіледі';
$_['text_success']                 = 'Сіздің сауалыңыз сәтті жіберілді. Жауап көрсетілген поштаға келеді';
$_['text_setting']                 = 'Параметрлер';
$_['text_reward']                  = 'Менің бонустарым';
$_['text_lotery'] 				   = 'Ұтыс ойынына тіркелу';
$_['text_instagram']			   = 'Instagram';
$_['text_office_address']		   = 'Қазақстан Республикасы, Алматы қ.,050000, Мақатаев к.,28/1';

// Enter
$_['enter_name']     = 'Сіздің атыңыз';
$_['enter_email']    = 'Сіздің E-Mail';
$_['enter_question'] = 'Сіздің сұрағыңыз';

// Error
$_['error_email']    = 'Дұрыс E-Mail енгізіңіз';
$_['error_question'] = 'Сұрақ кемінде 10 таңбаны қамтуы тиіс';
$_['error_send']     = 'Жіберу кезінде қате кетті. Кейінірек көріңіз';
