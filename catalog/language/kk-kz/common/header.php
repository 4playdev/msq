<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_home']          = 'Басты бет';
$_['text_wishlist']      = 'Таңдаулы';
$_['text_shopping_cart'] = 'Себет';
$_['text_category']      = 'Санаттар';
$_['text_account']       = 'Жеке <br class="d-md-none">кабинет';
$_['text_register']      = 'Тіркелу';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'Тапсырыс тарихы';
$_['text_transaction']   = 'Операциялар';
$_['text_download']      = 'Жүктеу файлдары';
$_['text_logout']        = 'Шығу';
$_['text_checkout']      = 'Тапсырысты рәсімдеу';
$_['text_search']        = 'Іздеу';
$_['text_instagram']     = 'Instagram';
$_['text_twitter']       = 'Twitter';
$_['text_vkontakte']     = 'ВКонтакте';
$_['text_home']          = 'Басты бет';
$_['text_latest']        = 'Жаңалықтар';
$_['text_catalog']       = 'Каталог';
$_['text_about_us']      = 'Біз туралы';
$_['text_help']          = 'Көмек';
$_['text_contact']       = 'Байланыс мәліметтері';
$_['text_lotery']		 = 'Ұтыс ойынына қатысу';