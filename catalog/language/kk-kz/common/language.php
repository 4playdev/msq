<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_language'] = 'Тіл';
$_['text_en-gb']    = 'Ағылшынша';
$_['text_en-en']    = 'Ағылшынша';
$_['text_ru-ru']    = 'Орысша';
$_['text_kk-kz']    = 'Қазақша';