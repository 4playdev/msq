<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Қызмет көрсету режимі';

// Text
$_['text_maintenance'] = 'Қызмет көрсету режимі';
$_['text_message']      = '<h1 style="text-align:center;">Дүкен уақытша жабық: алдын алу жұмыстары жүргізілуде.<br/>Көп ұзамай дүкен қол жетімді болады. Кейінірек кіріңіз.</h1>';