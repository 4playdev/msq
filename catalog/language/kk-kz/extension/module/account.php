<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Жеке кабинет';

// Text
$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Кіру';
$_['text_logout']      = 'Шығу';
$_['text_forgotten']   = 'Құпия сөзді ұмыттыңыз ба?';
$_['text_account']     = 'Менің ақпаратым';
$_['text_edit']        = 'Байланыс ақпаратын өзгерту';
$_['text_password']    = 'Құпия сөз';
$_['text_address']     = 'Байланыс мәліметтерінің тізімі';
$_['text_wishlist']    = 'Таңдаулы';
$_['text_order']       = 'Тапсырыс тарихы';
$_['text_download']    = 'Жүктеу файлдары';
$_['text_reward']      = 'Бонустық ұпайлар';
$_['text_return']      = 'Қайтарымдар';
$_['text_transaction'] = 'Төлемдер';
$_['text_newsletter']  = 'E-Mail тарату';
$_['text_recurring']   = 'Тұрақты төлемдер';