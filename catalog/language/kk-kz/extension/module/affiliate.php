<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Серіктестік бөлім';

// Text
$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Кіру';
$_['text_logout']      = 'Шығу';
$_['text_forgotten']   = 'Құпия сөзді ұмыттыңыз ба?';
$_['text_account']     = 'Менің ақпаратым';
$_['text_edit']        = 'Ақпаратты редакциялау';
$_['text_password']    = 'Құпия сөз';
$_['text_payment']     = 'Төлеу тәсілдері';
$_['text_tracking']    = 'Жолдама коды';
$_['text_transaction'] = 'Операциялар';
