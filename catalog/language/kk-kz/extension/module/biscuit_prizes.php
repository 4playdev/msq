<?php
    // KZ_kz
    $_['heading_title']     = 'Ұтыс ойынына тіркелу';
    $_['text_fname']        = 'Аты';
    $_['text_lname']        = 'Жөні';
    $_['text_instagram']    = 'Instagram';
    $_['text_email']        = 'E-mail мекенжайы';
    $_['text_phone']        = 'Телефон';
    $_['text_code']         = 'Код';
    $_['text_number']       = 'Сіздің нөміріңіз';
    $_['text_code_descr']   = 'Жапсырма кодын енгізіңіз';
    $_['text_error']        = 'Модуль қосылулы';
    $_['text_description']  = '';
	$_['text_send']         = 'Жіберу';
	$_['text_enter_code']   = 'Кодты теріңіз';
    $_['text_agree']        = '<a href="%s" class="agree link link--black font-weight-bold"><b>%s</b></a> оқып, шарттарымен келісемін.';

    $_['error_service_unavailable']     = 'Сервис временно не доступен, повторите попытку позже!';
    $_['error_number_wrong']            = 'Введен не верный Код, внимательно проверте все символы и повторите попытку позже!';
    $_['error_number_registered']       = 'Введеный Код уже зарегистрирован, воспользуйтесь формой провеки номеров!';
    $_['error_number_check']       = 'По указаному email участники не найдены, проверте корректность введенного email!';

    $_['accept_registration']           = 'Поздравляем!';
    $_['accept_registration_n']         = 'Ваш номер участника:';
    $_['accept_registration_d']         = 'Сохраните Ваш номер участника и следите за розыгрышем в <a href="%s"><b>Instagram</b></a>.';
    $_['accept_check_n']                = 'Ваши номера участника:';

    $_['new_reg']         = 'Продолжить регистрацию';
    $_['new_check']       = 'Продолжить проверку';