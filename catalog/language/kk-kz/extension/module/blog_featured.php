<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading 
$_['heading_title'] = 'Ұсынамыз';
$_['text_views'] 	= 'Қараулар:';
$_['button_more']   = 'толығырақ';

// Text
$_['text_reviews']  = '%s пікірлер негізінде.'; 