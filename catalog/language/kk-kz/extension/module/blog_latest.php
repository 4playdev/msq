<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Соңғы мақалалар';
$_['text_views'] 	= 'Көрсетілімдер:';
$_['button_more']   = 'толығырақ';

// Text
$_['text_reviews']  = '%s пікірлер негізінде.';