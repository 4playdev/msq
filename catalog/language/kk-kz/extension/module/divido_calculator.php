<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Calculator
$_['text_checkout_title']      = 'Бөліп төлеу';
$_['text_choose_plan']         = 'Өз жоспарыңызды таңдаңыз';
$_['text_choose_deposit']      = 'Өз депозитіңізді таңдаңыз';
$_['text_monthly_payments']    = 'ай сайынғы төлемдер';
$_['text_months']              = 'айлар';
$_['text_term']                = 'Мерзімі';
$_['text_deposit']             = 'Депозит';
$_['text_credit_amount']       = 'Несие құны';
$_['text_amount_payable']      = 'Төлеуге жиыны';
$_['text_total_interest']      = 'Пайыздардың жалпы мөлшері';
$_['text_monthly_installment'] = 'Ай сайынғы төлем';
$_['text_redirection']         = 'Сіз тапсырысыңызды растау кезінде осы қаржылық өтінімді аяқтау үшін менеджерге жіберілесіз';
