<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Оқуға кеңес береміз';
$_['text_views'] 	= 'Көрсетілімдер:';
$_['button_more']   = 'толығырақ';

// Text
$_['text_tax']      = 'НДС-сіз:';