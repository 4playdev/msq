<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Ақпарат';

// Text
$_['text_contact']  = 'Бізбен байланысу';
$_['text_sitemap']  = 'Сайт картасы';