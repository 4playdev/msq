<?php

// Text
$_['text_instagram'] = 'Instagram';
$_['text_twitter']   = 'Twitter';
$_['text_vkontakte'] = 'ВКонтакте';
$_['text_home']      = 'Басты бет';
$_['text_latest']    = 'Жаңалықтар';
$_['text_catalog']   = 'Каталог';
$_['text_about_us']  = 'Біз туралы';
$_['text_help']      = 'Көмек';
$_['text_contact']   = 'Байланысу';
$_['text_lotery']	 = 'Ұтыс ойынына тіркелу';