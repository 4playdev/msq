<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']  = 'Байланыс';

// Text
$_['text_location']  = 'Біздің мекенжайымыз:';
$_['text_store']     = 'Біздің дүкендеріміз';
$_['text_contact']   = 'Бізге жазу';
$_['text_address']   = 'Мекенжай';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Жұмыс тәртібі';
$_['text_comment']   = 'Қосымша ақпарат';
$_['text_email']     = 'E-Mail';
$_['text_map']       = 'Дүкендер картасы';
$_['text_success']   = '<p>Сіздің сұрауыңыз дүкен әкімшілігіне сәтті жіберілді!</p>';
$_['text_message']   = '<p>Сіздің сұрауыңыз дүкен әкімшілігіне сәтті жіберілді!</p>';
$_['text_office_address']		   = 'Қазақстан Республикасы, Алматы қ.,050000, Мақатаев к.,28/1';

// Entry
$_['entry_name']     = 'Сіздің атыңыз';
$_['entry_email']    = 'E-Mail мекенжай';
$_['entry_enquiry']  = 'Сіздің хабарламаңыз';

// Email
$_['email_subject']  = '%s хабарламасы';

// Errors
$_['error_name']     = 'Аты 3-32 таңбадан тұруы керек!';
$_['error_email']    = 'E-Mail мекенжайы дұрыс емес!';
$_['error_enquiry']  = 'Мәтін ұзындығы 10-нан 3000 таңбаға дейін болуы керек!';

// Button
$_['button_submit']  = 'Жіберу';
