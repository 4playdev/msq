<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Қалай тапсырыс беруге болады?';

// Text
$_['text_instruction_choose']   = 'Каталогтан ұнаған <br>тауарды таңдау';
$_['text_instruction_cart']     = '"Себетке салу" <br>түумешігіне басу';
$_['text_instruction_checkout'] = 'Төменде "тапсырысты ресімдеу" батырмасын басу арқылы <br>тапсырысты ресімдеуге өту.';