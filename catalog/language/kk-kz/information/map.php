<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Карта';
$_['heading_map_menu'] = 'Мекенжайлар';

// Text
$_['text_near']   = 'Жақын дүкен';
$_['text_search']     = 'Іздеу';
$_['text_address']     = 'Мекенжайлар';
