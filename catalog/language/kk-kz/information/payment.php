<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']           = 'Төлем';
$_['heading_payment_cash']    = 'Қолма-қол есеп айырысу';
$_['heading_payment_courier'] = 'Курьерге қолма-қол ақшасыз төлем';
$_['heading_payment_online']  = 'Сайттағы онлайн төлем';

// Text
$_['text_payment_cash']    = 'Оригинальная технологи Оригинальная технологи Оригинальная технологи Оригинальная <br>технологи Оригинальная технологи';
$_['text_payment_courier'] = 'Оригинальная технологи Оригинальная технологи Оригинальная технологи Оригинальная <br>технологи Оригинальная технологи';
$_['text_payment_online']  = 'У вас всегда есть возможность сделать предоплату за свой заказ, при помощи банковской карты, <br>при его оформлении. <br>При предоплате заказа, доставка будет бесплатной.';