<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Сайт картасы';

// Text
$_['text_special']     = 'Акциялар';
$_['text_account']     = 'Жеке кабинет';
$_['text_edit']        = 'Жеке ақпарат';
$_['text_password']    = 'Құпия сөз';
$_['text_address']     = 'Менің мекенжайларым';
$_['text_history']     = 'Тапсырыс тарихы';
$_['text_download']    = 'Жүктеу файлдары';
$_['text_cart']        = 'Сатып алулар себеті';
$_['text_checkout']    = 'Тапсырысты рәсімдеу';
$_['text_search']      = 'Іздеу';
$_['text_information'] = 'Ақпарат';
$_['text_contact']     = 'Байланыс';