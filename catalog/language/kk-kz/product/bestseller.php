<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']     = 'Сатылым Хиттары';

// Text
$_['text_empty']        = 'Сатылым Хиттары жоқ.';
$_['text_quantity']     = 'Саны:';
$_['text_manufacturer'] = 'Өндіруші:';
$_['text_model']        = 'Тауар Коды:';
$_['text_points']       = 'Банусты ұпайлар:';
$_['text_price']        = 'Бағасы:';
$_['text_tax']          = 'ҚҚС-сыз:';
$_['text_compare']      = 'Тауарларды салыстыру (%s)';
$_['text_sort']         = 'Сұрыптау:';
$_['text_default']      = 'Әдетті';
$_['text_name_asc']     = 'Атауы (А - Я)';
$_['text_name_desc']    = 'Атауы (Я - А)';
$_['text_price_asc']    = 'Бағасы (төмен &gt; жоғары)';
$_['text_price_desc']   = 'Бағасы (төмен &gt; жоғары)';
$_['text_rating_asc']   = 'Рейтинг (төменнен бастап)';
$_['text_rating_desc']  = 'Рейтинг (жоғарыдан бастап)';
$_['text_model_asc']    = 'Тауар Коды (А - Я)';
$_['text_model_desc']   = 'Тауар Коды (Я - А)';
$_['text_limit']        = 'Көрсету:';
$_['text_benefits']     = 'Артықшылықтары:';