<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_search']              = 'Іздеу';
$_['text_brand']               = 'Өндіруші';
$_['text_manufacturer']        = 'Өндіруші:';
$_['text_model']               = 'Тауар Коды:';
$_['text_reward']              = 'Бонусты ұпайлар:';
$_['text_points']              = 'Бонустық ұпайдағы баға:';
$_['text_stock']               = 'Қолда бары:';
$_['text_instock']             = 'Қолда бары';
$_['text_tax']                 = 'ҚҚС-сыз:';
$_['text_discount']            = ' немесе көбірек ';
$_['text_option']              = 'Қол жетімді варианттар';
$_['text_minimum']             = 'Бұл тауардың ең аз тапсырыс саны %s';
$_['text_reviews']             = '%s пікір';
$_['text_write']               = 'Пікір жазу';
$_['text_login']               = 'Пікір жазбас бұрын,  <a href= "%s ">авторизацияланыңыз< / a> немесе <a href= "%s">тіркелгіні жасаңыз</a>';
$_['text_no_reviews']          = 'Бұл тауар туралы пікірлер жоқ.';
$_['text_note']                = '<span style="color: #FF0000;">Ескертпе:</span> HTML таңбаға қолдау көрсетілмейді! Қалыпты мәтінді пайдаланыңыз.';
$_['text_success']             = 'Пікір жазғаныңыз үшін рахмет. Ол спам тексеру үшін әкімшіге келіп, көп ұзамай жарияланады.';
$_['text_related']             = 'Ұсынылатын';
$_['text_tags']                = 'Тегтер:';
$_['text_error']               = 'Тауар табылмады!';
$_['text_payment_recurring']   = 'Төлем профилі';
$_['text_trial_description']   = 'Сома: %s; Кезеңділігі: %d %s; Төлемдер саны: %d, Ары қарай ';;
$_['text_payment_description'] = 'Сома: %s; Кезеңділігі:  %d %s; Төлемдер саны:  %d ';
$_['text_payment_cancel']      = 'Сома: %s; Кезеңділігі:  %d %s(s) ; Төлемдер саны: күші жойылғанға дейін';
$_['text_day']                 = 'Күні';
$_['text_week']                = 'Аптасы';
$_['text_semi_month']          = 'Жарты ай';
$_['text_month']               = 'Ай';
$_['text_year']                = 'Жыл';
$_['text_review']              = 'Пікірлер';
$_['text_rating_avg']          = 'Орташа бағасы';
$_['text_benefits']     	   = 'Артықшылығы:';
$_['text_cakes']			   = 'Торттар';
$_['text_desc_cakes']		   = '*Торттың бағасы оның салмағына байланысты.';

// Entry
$_['entry_qty']                = 'Саны';
$_['entry_name']               = 'Атыңыз';
$_['entry_review']             = 'Пікіріңіз';
$_['entry_rating']             = 'Рейтингі';
$_['entry_good']               = 'Жақсы';
$_['entry_bad']                = 'Жаман';

// Tabs
$_['tab_description']          = 'Сипаттамасы';
$_['tab_attribute']            = 'Характеристикасы';
$_['tab_review']               = 'Пікір саны (%s)';

// Error
$_['error_name']               = 'Имя должно содержать от 3 до 25 символов!';
$_['error_text']               = 'Текст отзыва должен содержать от 25 до 1000 символов!';
$_['error_rating']             = 'Пожалуйста, выберите оценку!';