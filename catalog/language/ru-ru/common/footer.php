<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_information']             = 'Информация';
$_['text_service']                 = 'Служба поддержки';
$_['text_extra']                   = 'Дополнительно';
$_['text_contact']                 = 'Связаться с нами';
$_['text_address'] 	               = 'Адрес';
$_['text_questions'] 	           = 'Вопросы';
$_['text_return']                  = 'Возврат товара';
$_['text_sitemap']                 = 'Карта сайта';
$_['text_manufacturer']            = 'Производители';
$_['text_voucher']                 = 'Подарочные сертификаты';
$_['text_affiliate']               = 'Партнёрская программа';
$_['text_special']                 = 'Акции';
$_['text_latest']                  = 'Новинки';
$_['text_bestseller']              = 'Хиты Продаж';
$_['text_mostviewed']              = 'Популярные Товары';
$_['text_account']                 = 'Аккаунт';
$_['text_my_account']              = 'Мой аккаунт';
$_['text_order']                   = 'История заказа';
$_['text_wishlist']                = 'Избранное';
$_['text_newsletter']              = 'Рассылка';
$_['text_about_us']                = 'О нас';
$_['text_delivery']                = 'Доставка';
$_['text_addresses']               = 'Наши адреса';
$_['text_question_order']          = 'Как сделать заказ?';
$_['text_question_payment_online'] = 'Онлайн оплата';
$_['text_question_own']            = 'Свой вопрос';
$_['text_powered']                 = 'Работает на <a target="_blank" href="https://ocstore.com/">ocStore</a><br /> %s &copy; %s';
$_['text_send']                    = 'Отправить';
$_['text_email_notice']            = 'Ответ будет выслан Вам на эту почту';
$_['text_success']                 = 'Ваш вопрос успешно отправлен. Ответ придет на указанную почту';
$_['text_setting']                 = 'Настройки';
$_['text_reward']                  = 'Мои бонусы';
$_['text_instagram']			   = 'Instagram';
$_['text_lotery'] 				   = 'Регистрация в розыгрыше';
$_['text_office_address']		   = 'Республика Казахстан, г. Алматы, 050000, ул. Макатаева, 28/1';

// Enter
$_['enter_name']     = 'Ваше имя';
$_['enter_email']    = 'Ваш E-Mail';
$_['enter_question'] = 'Ваш вопрос';

// Error
$_['error_email']    = 'Пожалуйста, введите корректный E-Mail';
$_['error_question'] = 'Вопрос должен содержать минимум 10 символов';
$_['error_send']     = 'Произошла ошибка при отправке. Попробуйте позже';
