<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Избранное';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный <br class="d-md-none">кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказа';
$_['text_transaction']   = 'Операции';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_instagram']     = 'Instagram';
$_['text_twitter']       = 'Twitter';
$_['text_vkontakte']     = 'ВКонтакте';
$_['text_home']          = 'Главная';
$_['text_latest']        = 'Новинки';
$_['text_catalog']       = 'Каталог';
$_['text_about_us']      = 'О нас';
$_['text_help']          = 'Помощь';
$_['text_contact']       = 'Контакты';
$_['text_lotery']		 = 'Регистрация в розыгрыше';