<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_language'] = 'Язык';
$_['text_en-gb']    = 'Английский';
$_['text_en-en']    = 'Английский';
$_['text_ru-ru']    = 'Русский';
$_['text_kk-kz']    = 'Казахский';