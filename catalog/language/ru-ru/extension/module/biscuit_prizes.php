<?php
    // RU_ru
    $_['heading_title']         = 'Регистрация в розыгрыше';
    $_['text_fname']            = 'Имя';
    $_['text_lname']            = 'Фамилия';
    $_['text_instagram']        = 'Instagram';
    $_['text_email']            = 'E-mail адрес';
    $_['text_phone']            = 'Телефон';
    $_['text_code']             = 'Код';
    $_['text_number']           = 'Ваш номер';
    $_['text_code_descr']       = 'Введите код с наклейки';
    $_['text_error']            = 'Модуль выключен';
    $_['text_description']      = '';
	$_['text_send']             = 'Отправить';
	$_['text_enter_code']       = 'Введите код';
    $_['text_agree']            = 'Я прочитал и согласен с условиями <a href="%s" class="agree link link--black font-weight-bold"><b>%s</b></a>';

    $_['error_service_unavailable']     = 'Сервис временно не доступен, повторите попытку позже!';
    $_['error_number_wrong']            = 'Введен не верный Код, внимательно проверте все символы и повторите попытку позже!';
    $_['error_number_registered']       = 'Введеный Код уже зарегистрирован, воспользуйтесь формой провеки номеров!';
    $_['error_number_check']       = 'По указаному email участники не найдены, проверте корректность введенного email!';

    $_['accept_registration']           = 'Поздравляем!';
    $_['accept_registration_n']         = 'Ваш номер участника:';
    $_['accept_registration_d']         = 'Сохраните Ваш номер участника и следите за розыгрышем в <a href="%s"><b>Instagram</b></a>.';
    $_['accept_check_n']                = 'Ваши номера участника:';

    $_['new_reg']         = 'Продолжить регистрацию';
    $_['new_check']       = 'Продолжить проверку';