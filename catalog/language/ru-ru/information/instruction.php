<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Как сделать заказ';

// Text
$_['text_instruction_choose']   = 'Выбрать понравившейся <br>товар из каталога';
$_['text_instruction_cart']     = 'Нажать на кнопку <br>«Добавить в корзину»';
$_['text_instruction_checkout'] = 'Перейти к оформлению заказа, <br>нажав кнопку «Оформить заказ» внизу.';