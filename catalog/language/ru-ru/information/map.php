<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Карта';
$_['heading_map_menu'] = 'Адреса';

// Text
$_['text_near']   = 'Магазин рядом';
$_['text_search']     = 'Поиск';
$_['text_address']     = 'Адреса';

