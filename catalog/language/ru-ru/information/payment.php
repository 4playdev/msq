<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']           = 'Оплата';
$_['heading_payment_cash']    = 'Наличный расчет';
$_['heading_payment_courier'] = 'Безналичная оплата курьеру';
$_['heading_payment_online']  = 'Онлайн-оплата на сайте';

// Text
$_['text_payment_cash']    = 'Оригинальная технологи Оригинальная технологи Оригинальная технологи Оригинальная <br>технологи Оригинальная технологи';
$_['text_payment_courier'] = 'Оригинальная технологи Оригинальная технологи Оригинальная технологи Оригинальная <br>технологи Оригинальная технологи';
$_['text_payment_online']  = 'У вас всегда есть возможность сделать предоплату за свой заказ, при помощи банковской карты, <br>при его оформлении. <br>При предоплате заказа, доставка будет бесплатной.';