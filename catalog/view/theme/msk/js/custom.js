var gotop_btn_show_point = 300,
	gotop_btn = "",
	gotop_btn_class = 'gotop-btn',
	gotop_btn_show_class = gotop_btn_class + "--show";

var top_toast;
var top_categories;

function gotop() {
	$("body, html").animate({
		scrollTop: 0
	}, 'slow');
}

function gotop_btn_show(){
	if(window.scrollY > gotop_btn_show_point)
		gotop_btn.addClass(gotop_btn_show_class);
	else
		gotop_btn.removeClass(gotop_btn_show_class);
}

function fancybox_init(){
	$('.fancybox-item').fancybox({
		loop: true,
		toolbar: true,
		buttons: ["zoom", "close"],
		zoom: true,
	});
}

function show_input_error_modal(input, error_message){
	if(!input instanceof jQuery) input = $(input);

	var error_block = input.prev('.error-block-modal');

	if(error_block.length === 0){
		input.before('<div class="error-block-modal"></div>');
		error_block = input.prev('.error-block-modal');
	}

	input
		.focus()
		.addClass('error');
	setTimeout(function () {
		error_block
			.text(error_message)
			.addClass('show');
	}, 100);
	setTimeout(function () {
		error_block.removeClass('show');
		input.removeClass('error');
	}, 5000);
}

function valid_email(email){
	return /^.{1,}\@.{2,}\..{2,}$/.test(email);
}

function show_top_toast(text, type) {
	var types = ['error', 'success'],
		message = "";

	$.each(types, function (index, type) {
		top_toast.removeClass(type);
	});

	if(text instanceof Array || text instanceof Object){
		$.each(text, function (index, error_items) {
			$.each(error_items, function (index2, error) {
				if(message !== "")
					message = message + "<br>";
				message = message + error;
			});
		})
	}
	else
		message = text;

	top_toast
		.addClass('show ' + type)
		.find('.top-toast__text')
		.html(message);
}

function hide_top_toast() {
	top_toast.removeClass('show');
}

function disable_button(button) {
	if(!(button instanceof jQuery)) button = $(button);

	button.attr('disabled', 'disabled');
}

function enable_button(button) {
	if(!(button instanceof jQuery)) button = $(button);

	button.removeAttr('disabled');
}

function show_sort_popup(){
	$('.sort__list').addClass('sort__list--show');
}

function hide_sort_popup(target){
	var sort_block = $('.sort'),
		list_block = $('.sort__list');

	if (!sort_block.is(target) && sort_block.has(target).length === 0 && list_block.hasClass('sort__list--show'))
		list_block.removeClass('sort__list--show');
}

window.addEventListener('load', function () {
	gotop_btn = $('.' + gotop_btn_class);
	top_toast = $('.top-toast');
	top_categories = $('.top-menu .categories-list');

	setTimeout(function () {
		gotop_btn_show();
		fancybox_init();

		gotop_btn.on('click', gotop);

		window.addEventListener('scroll', function () {
			gotop_btn_show();
		});
	}, 100);


	$('.show-catalog-link').on('click', function (event) {
		event.preventDefault();

		top_categories.addClass('categories-list--show');
	});

	$(document).mousedown(function(event) {
		if (!top_categories.is(event.target) && top_categories.has(event.target).length === 0) {
			top_categories.removeClass('categories-list--show');
		}

		hide_sort_popup(event.target);
	});

	$('.sort__btn').on('click', show_sort_popup);

	$('.mobile-menu-btn').on('click', function () {
		$('.mobile-menu').addClass('mobile-menu--show');
	});

	$('.mobile-menu__close-btn').on('click', function () {
		$('.mobile-menu').removeClass('mobile-menu--show');
	});

	$('.show-catalog-items').on('click', function () {
		var block = $(this),
			categories = block.find('.mobile-menu__categories');
		if(block.hasClass('show-catalog-items--show'))
			categories.hide();
		else
			categories.show();

		block.toggleClass('show-catalog-items--show');
	});
});

